<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_donl_connect\Functional\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\xs_donl_connect\Form\ModuleSettingsForm;
use Drupal\xs_lists\ListClient;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Tests\xs_donl_connect\TestCase;

/**
 * @internal
 */
final class ModuleSettingsFormTest extends TestCase
{
  public function testFormCanBeCreated(): void
  {
    try {
      Assert::assertInstanceOf(
        ModuleSettingsForm::class,
        ModuleSettingsForm::create($this->mockContainerInterface())
      );
    } catch (ServiceCircularReferenceException|ServiceNotFoundException $e) {
      Assert::fail($e->getMessage());
    }
  }

  public function testFormIdIsNotEmpty(): void
  {
    $form = ModuleSettingsForm::create($this->mockContainerInterface());

    Assert::assertNotEmpty($form->getFormId());
  }

  private function mockContainerInterface(): ContainerInterface
  {
    return M::mock(ContainerInterface::class, function(MI $mock) {
      $configFactory    = M::mock(ConfigFactoryInterface::class);
      $listClient       = M::mock(ListClient::class);
      $routeBuilder     = M::mock(RouteBuilderInterface::class);
      $cacheInvalidator = M::mock(CacheTagsInvalidatorInterface::class);

      $mock->shouldReceive('get')->with('config.factory')->andReturn($configFactory);
      $mock->shouldReceive('get')->with('xs_lists.list_client')->andReturn($listClient);
      $mock->shouldReceive('get')->with('router.builder')->andReturn($routeBuilder);
      $mock->shouldReceive('get')->with('cache_tags.invalidator')->andReturn($cacheInvalidator);
    });
  }
}
