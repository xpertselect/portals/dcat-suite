<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_donl_connect\Unit;

use Drupal\xs_donl_connect\XsDonlConnect;
use PHPUnit\Framework\Assert;
use Tests\xs_donl_connect\TestCase;

/**
 * @internal
 */
final class XsDonlConnectTest extends TestCase
{
  public static function privateDatasetDataset(): array
  {
    return [
      'no-key' => [FALSE, ['foo' => 'bar']],
      'falsy'  => [FALSE, ['private' => FALSE]],
      'string' => [FALSE, ['private' => 'TRUE']],
      'truthy' => [TRUE, ['private' => TRUE]],
    ];
  }

  /**
   * @dataProvider privateDatasetDataset
   */
  public function testDatasetIsPrivate(bool $isPrivate, array $dataset): void
  {
    Assert::assertEquals($isPrivate, XsDonlConnect::datasetIsPrivate($dataset));
  }
}
