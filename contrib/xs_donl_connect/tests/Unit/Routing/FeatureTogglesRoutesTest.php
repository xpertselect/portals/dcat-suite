<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_donl_connect\Unit\Routing;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\xs_donl_connect\Routing\FeatureToggledRoutes;
use Drupal\xs_donl_connect\XsDonlConnect;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_donl_connect\TestCase;

/**
 * @internal
 */
final class FeatureTogglesRoutesTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    $configFactory = M::mock(ConfigFactoryInterface::class, function(MI $mock) {
      $config = M::mock(Config::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->andReturn(FALSE);
      });

      $mock->shouldReceive('get')
        ->with(XsDonlConnect::SETTINGS_KEY)
        ->andReturn($config);
    });

    Assert::assertInstanceOf(
      FeatureToggledRoutes::class,
      FeatureToggledRoutes::create($configFactory)
    );
  }

  public function testInstanceCanBeCreatedWhenConfigIsMissing(): void
  {
    $configFactory = M::mock(ConfigFactoryInterface::class, function(MI $mock) {
      $config = M::mock(Config::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->andReturn(NULL);
      });

      $mock->shouldReceive('get')
        ->with(XsDonlConnect::SETTINGS_KEY)
        ->andReturn($config);
    });

    Assert::assertInstanceOf(
      FeatureToggledRoutes::class,
      FeatureToggledRoutes::create($configFactory)
    );
  }

  public function testNoRoutesAreOfferedWhenFeatureIsDisabled(): void
  {
    $instance = new FeatureToggledRoutes(FALSE);

    Assert::assertCount(0, $instance->getRoutes());
  }

  public function testRoutesAreOfferedWhenFeatureIsEnabled(): void
  {
    $instance = new FeatureToggledRoutes(TRUE);

    Assert::assertGreaterThan(0, count($instance->getRoutes()));
  }
}
