<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_donl_connect\Unit\EventSubscriber;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\xs_donl_connect\EventSubscriber\AlterViewDatasetSubscriber;
use Drupal\xs_donl_connect\XsDonlConnect;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_donl_connect\TestCase;

/**
 * @internal
 */
final class AlterViewDatasetSubscriberTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    $configFactory = M::mock(ConfigFactoryInterface::class, function(MI $mock) {
      $config = M::mock(Config::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->with('enable')
          ->andReturn(TRUE);
      });

      $mock->shouldReceive('get')
        ->with(XsDonlConnect::SETTINGS_KEY)
        ->andReturn($config);
    });

    Assert::assertInstanceOf(
      AlterViewDatasetSubscriber::class,
      AlterViewDatasetSubscriber::create($configFactory)
    );
  }

  public function testSubscriberListensToEvents(): void
  {
    Assert::assertNotEmpty(AlterViewDatasetSubscriber::getSubscribedEvents());
  }
}
