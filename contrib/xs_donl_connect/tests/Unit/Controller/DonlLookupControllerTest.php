<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_donl_connect\Unit\Controller;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_donl_connect\Controller\DonlLookupController;
use Drupal\xs_donl_connect\XsDonlConnect;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_donl_connect\TestCase;

/**
 * @internal
 */
final class DonlLookupControllerTest extends TestCase
{
  public function testControllerCanBeCreated(): void
  {
    $container = M::mock(ContainerInterface::class, function(MI $mock) {
      $mock->shouldReceive('get')
        ->with('xs_donl_connect.ckan.sdk')
        ->andReturn(M::mock(CkanSdk::class));

      $mock->shouldReceive('get')
        ->with('cache.default')
        ->andReturn(M::mock(CacheBackendInterface::class));

      $mock->shouldReceive('get')
        ->with('config.factory')
        ->andReturn(M::mock(ConfigFactoryInterface::class, function(MI $mock) {
          $config = M::mock(Config::class, function(MI $mock) {
            $mock->shouldReceive('get')
              ->with('metadata.link_pattern')
              ->andReturn('foo');

            $mock->shouldReceive('get')
              ->with('connection.cache_duration')
              ->andReturn(5);
          });

          $mock->shouldReceive('get')
            ->with(XsDonlConnect::SETTINGS_KEY)
            ->andReturn($config);
        }));
    });

    Assert::assertInstanceOf(
      DonlLookupController::class,
      DonlLookupController::create($container)
    );
  }
}
