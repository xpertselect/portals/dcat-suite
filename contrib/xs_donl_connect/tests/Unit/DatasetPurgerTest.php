<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_donl_connect\Unit;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_donl_connect\DatasetPurger;
use Drupal\xs_donl_connect\XsDonlConnect;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\Log\LoggerInterface;
use Tests\xs_donl_connect\TestCase;

/**
 * @internal
 */
final class DatasetPurgerTest extends TestCase
{
  public static function serviceConfigDataset(): array
  {
    return [
      [FALSE, ''],
      [TRUE, 'foo'],
    ];
  }

  public function testServiceCanBeCreated(): void
  {
    $service = DatasetPurger::create(
      M::mock(CkanSdk::class),
      M::mock(ConfigFactoryInterface::class, function(MI $mock) {
        $config = M::mock(Config::class, function(MI $mock) {
          $mock->shouldReceive('get')
            ->with('connection.user_id')
            ->andReturn('foo');
        });

        $mock->shouldReceive('get')
          ->with(XsDonlConnect::SETTINGS_KEY)
          ->andReturn($config);
      }),
      M::mock(LoggerChannelFactoryInterface::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->with(XsDonlConnect::LOG_CHANNEL)
          ->andReturn(M::mock(LoggerInterface::class));
      })
    );

    Assert::assertInstanceOf(DatasetPurger::class, $service);
  }

  /**
   * @dataProvider serviceConfigDataset
   */
  public function testServiceState(bool $shouldBeHealthy, string $ckanUserId): void
  {
    $donlSdk = M::mock(CkanSdk::class);
    $logger  = M::mock(LoggerInterface::class);

    $service = new DatasetPurger(
      $donlSdk,
      $ckanUserId,
      $logger
    );

    Assert::assertEquals($shouldBeHealthy, $service->isHealthy());
  }
}
