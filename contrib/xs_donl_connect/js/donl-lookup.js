/* globals Drupal, jQuery, drupalSettings */

(function ($, Drupal, drupalSettings) {
  "use strict";

  let donlLookupPerformed = false;

  function getLocalDatasetName() {
    const datasetContainer = document.getElementById("datasetContainer");
    const datasetNameAttribute = "data-dataset-name";

    if (datasetContainer && datasetContainer.hasAttribute(datasetNameAttribute)) {
      return datasetContainer.getAttribute(datasetNameAttribute);
    }

    return null;
  }

  function insertDonlReference(datasetName) {
    if (null === datasetName || donlLookupPerformed) {
      return;
    }

    $.ajax({
      method: "GET",
      url: drupalSettings.xs_donl_connect.donl_lookup_url.replace("PLACEHOLDER", datasetName),
      accepts: {"Content-Type": "application/json"},
      success: function (data) {
        donlLookupPerformed = true;

        const datasetKeywordContainer = document.getElementById("datasetKeywordContainer");

        if (data.location && datasetKeywordContainer) {
          let firstKeyword = null;

          if (datasetKeywordContainer.hasChildNodes()) {
            firstKeyword = datasetKeywordContainer.firstChild;
          }

          datasetKeywordContainer.insertBefore(createDonlChipMarkup(data.location), firstKeyword);

          if (window.addToolTipToElement) {
            window.addToolTipToElement(datasetKeywordContainer.firstElementChild);
          }
        }
      },
    });
  }

  function createDonlChipMarkup(link) {
    let fragment = document.createElement("template");
    let title = Drupal.t("View this dataset on the Dutch national dataportal.");

    fragment.innerHTML = `
      <a href="${link}" target="_blank" title="${title}" data-bs-toggle="tooltip" data-placement="top" rel="noopener">
        <div class="chip donl-chip">
          <img src="/modules/contrib/xsp_dcat_suite/contrib/xs_donl_connect/images/overheid_nl.webp" aria-hidden="true" 
               alt="data.overheid.nl logo">
          Data.Overheid.nl
        </div>
      </a>
    `;

    return fragment.content;
  }

  Drupal.behaviors.donlLookup = {
    attach: function () {
      insertDonlReference(getLocalDatasetName());
    }
  };
})(jQuery, Drupal, drupalSettings);
