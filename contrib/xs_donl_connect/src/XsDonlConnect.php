<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect;

/**
 * Class XsDonlConnect.
 *
 * Simple PHP class holding various constants used by the `xs_donl_connect` Drupal module.
 */
final class XsDonlConnect
{
  /**
   * The key identifying the `xs_donl_connect` settings in Drupal.
   */
  public const SETTINGS_KEY = 'xs_donl_connect.settings';

  /**
   * The log channel that should be used by this Drupal module.
   */
  public const LOG_CHANNEL = 'xs_donl_connect';

  /**
   * Determine if the given dataset is considered private.
   *
   * @param array<string, mixed> $dataset The dataset to check
   *
   * @return bool Whether the dataset is private
   */
  public static function datasetIsPrivate(array $dataset): bool
  {
    return array_key_exists('private', $dataset) && TRUE === $dataset['private'];
  }
}
