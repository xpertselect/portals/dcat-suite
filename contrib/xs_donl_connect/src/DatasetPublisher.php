<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\xs_ckan\CkanSdk;
use Psr\Log\LoggerInterface;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class DatasetPublisher.
 *
 * A Drupal service for publishing datasets on the Dutch national dataportal.
 */
final readonly class DatasetPublisher
{
  /**
   * Creates a new DatasetPublisher instance.
   *
   * @param CkanSdk                       $donlSdk              The SDK for interacting with the API of the Dutch
   *                                                            national dataportal
   * @param ConfigFactoryInterface        $configFactory        The Drupal configuration factory
   * @param LoggerChannelFactoryInterface $loggerChannelFactory The Drupal logger factory
   *
   * @return self The created instance
   */
  public static function create(CkanSdk $donlSdk, ConfigFactoryInterface $configFactory,
                                LoggerChannelFactoryInterface $loggerChannelFactory): self
  {
    $config = $configFactory->get(XsDonlConnect::SETTINGS_KEY);

    return new self(
      $donlSdk,
      trim($config->get('metadata.name_prefix') ?? ''),
      trim($config->get('connection.owner_org') ?? ''),
      trim($config->get('metadata.source_catalog') ?? ''),
      trim($config->get('connection.user_id') ?? ''),
      $loggerChannelFactory->get(XsDonlConnect::LOG_CHANNEL)
    );
  }

  /**
   * DatasetPublisher constructor.
   *
   * @param CkanSdk         $donlSdk       The SDK for interacting with the API of the Dutch national dataportal
   * @param string          $namePrefix    The prefix to use when generating the name of a dataset
   * @param string          $ownerOrg      The ID of the CKAN organization that holds the published datasets
   * @param string          $sourceCatalog The sourceCatalog value to assign to the datasets
   * @param string          $userId        The CKAN user ID that owns the published datasets
   * @param LoggerInterface $logger        The logger the use
   */
  public function __construct(private CkanSdk $donlSdk, private string $namePrefix,
                              private string $ownerOrg, private string $sourceCatalog,
                              private string $userId, private LoggerInterface $logger)
  {
  }

  /**
   * Determine if this service is in a healthy state.
   *
   * @return bool Whether the service is healthy
   */
  public function isHealthy(): bool
  {
    if (empty($this->ownerOrg)) {
      return FALSE;
    }

    if (empty($this->sourceCatalog)) {
      return FALSE;
    }

    if (empty($this->userId)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Publish (create or update) a dataset on the Dutch national dataportal.
   *
   * @param array<string, mixed> $dataset The dataset to publish
   *
   * @return bool Whether publishing was successful
   */
  public function publishDataset(array $dataset): bool
  {
    $package = $this->prepareDatasetMetadata($dataset);

    try {
      $datasetRepository = $this->donlSdk->datasets();

      if ($publishedPackage = $this->getPublishedDataset($package)) {
        $datasetRepository->update($publishedPackage['id'], array_merge($package, [
          'name' => $publishedPackage['name'],
        ]));
      } else {
        $datasetRepository->create(array_merge($package, [
          'name' => $datasetRepository->generatePackageName($package['title'], $this->namePrefix),
        ]));
      }

      return TRUE;
    } catch (ClientException|ResponseException $e) {
      $message = $e->getMessage() ?: 'n/a';

      if ($e instanceof ResponseException) {
        $message = $e->response->getPsrResponse()->getBody()->getContents();
      }

      $this->logger->error(sprintf(
        'Failed to publish dataset "%s" to the Dutch national dataportal; %s',
        $package['title'],
        $message
      ));

      return FALSE;
    }
  }

  /**
   * Prepare the metadata of the local dataset such that it can be published on the Dutch national dataportal.
   *
   * @param array<string, mixed> $localDataset The local dataset to publish
   *
   * @return array<string, mixed> The updated local dataset
   */
  private function prepareDatasetMetadata(array $localDataset): array
  {
    $dataset = $localDataset;

    $this->removeDatasetKeys($dataset);
    $this->removeDistributionKeys($dataset);
    $this->flattenKeywords($dataset);
    $this->setOwnershipAndSourceValues($dataset);

    return $dataset;
  }

  /**
   * Remove specific attributes of the given dataset.
   *
   * @param array<string, mixed> $dataset The dataset to alter
   */
  private function removeDatasetKeys(array &$dataset): void
  {
    $keys = [
      'num_tags',
      'id',
      'type',
      'num_resources',
      'state',
      'groups',
      'creator_user_id',
      'organization',
      'isopen',
      'changetype',
      'revision_id',
      'metadata_created',
      'metadata_updated',
      'owner_org',
    ];

    foreach ($keys as $key) {
      unset($dataset[$key]);
    }
  }

  /**
   * Remove specific attributes of the resources of the given dataset.
   *
   * @param array<string, mixed> $dataset The dataset to alter
   */
  private function removeDistributionKeys(array &$dataset): void
  {
    $keys = [
      'id',
      'package_id',
      'metadata_created',
      'metadata_updated',
    ];

    foreach ($dataset['resources'] ?? [] as &$resource) {
      foreach ($keys as $key) {
        unset($resource[$key]);
      }
    }
  }

  /**
   * Flatten the keywords of the given dataset into the "tag_string" attribute.
   *
   * @param array<string, mixed> $dataset The dataset to alter
   */
  private function flattenKeywords(array &$dataset): void
  {
    $dataset['tag_string'] = implode(', ', array_filter(array_map(function(array $tag) {
      return $tag['display_name'] ?? $tag['name'] ?? NULL;
    }, $dataset['tags'] ?? [])));

    unset($dataset['tags']);
  }

  /**
   * Set the "owner_org" and "source_catalog" properties of the give dataset.
   *
   * @param array<string, mixed> $dataset The dataset to alter
   */
  private function setOwnershipAndSourceValues(array &$dataset): void
  {
    $dataset['owner_org']      = $this->ownerOrg;
    $dataset['source_catalog'] = $this->sourceCatalog;
  }

  /**
   * Retrieve the published version of the given dataset, if there is one.
   *
   * @param array<string, mixed> $dataset The dataset as it is published on the Dutch national dataportal
   *
   * @return null|array<string, mixed> The published dataset or null if there is none
   *
   * @throws ResponseException Throw on any error when communicating with the Dutch national dataportal
   * @throws ClientException   Throw on any error when communicating with the Dutch national dataportal
   */
  private function getPublishedDataset(array $dataset): ?array
  {
    $searchResults = $this->donlSdk->datasets()->search([
      'rows' => 1,
      'q'    => sprintf('identifier:"%s" AND creator_user_id:"%s"', $dataset['identifier'], $this->userId),
    ]);

    return $searchResults['results'][0] ?? NULL;
  }
}
