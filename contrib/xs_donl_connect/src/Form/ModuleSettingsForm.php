<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\xs_donl_connect\XsDonlConnect;
use Drupal\xs_lists\ListClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ModuleSettingsForm.
 *
 * A Drupal configuration form for managing the settings of the xs_donl_connect module.
 */
final class ModuleSettingsForm extends ConfigFormBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    /** @var ListClient $listClient */
    $listClient = $container->get('xs_lists.list_client');

    /** @var RouteBuilderInterface $routeBuilder */
    $routeBuilder = $container->get('router.builder');

    /** @var CacheTagsInvalidatorInterface $cacheTagsInvalidator */
    $cacheTagsInvalidator = $container->get('cache_tags.invalidator');

    return new self($configFactory, $listClient, $routeBuilder, $cacheTagsInvalidator);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, protected readonly ListClient $listClient,
                              protected readonly RouteBuilderInterface $routeBuilder,
                              protected readonly CacheTagsInvalidatorInterface $cacheTagsInvalidator)
  {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'xs_donl_connect_module_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $currentConfig = $this->config(XsDonlConnect::SETTINGS_KEY);

    $form['enable'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable feature'),
      '#description'   => $this->t('Enable the near real-time synchronization of DCAT content to the Dutch national dataportal.'),
      '#default_value' => $currentConfig->get('enable'),
      '#return_value'  => 'checked',
    ];

    $form['connection'] = [
      '#type'                => 'fieldset',
      '#title'               => $this->t('Dutch national dataportal'),
      '#description'         => $this->t('Settings related to how the connection will be made to the Dutch national dataportal.'),
      '#description_display' => 'before',

      'endpoint' => [
        '#type'                => 'url',
        '#title'               => $this->t('CKAN API endpoint'),
        '#description'         => $this->t('The HTTP address of the CKAN API of the Dutch national dataportal.'),
        '#description_display' => 'before',
        '#default_value'       => $currentConfig->get('connection.endpoint'),
        '#placeholder'         => 'e.g. https://data.overheid.nl/data',
      ],

      'owner_org' => [
        '#type'                => 'textfield',
        '#title'               => $this->t('CKAN Organization'),
        '#description'         => $this->t('The CKAN organization under which all DCAT objects will be published on the Dutch national dataportal.'),
        '#description_display' => 'before',
        '#default_value'       => $currentConfig->get('connection.owner_org'),
        '#placeholder'         => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
      ],

      'user_id' => [
        '#type'                => 'textfield',
        '#title'               => $this->t('CKAN User ID'),
        '#description'         => $this->t('The ID of the CKAN user which is used for all API interactions.'),
        '#description_display' => 'before',
        '#default_value'       => $currentConfig->get('connection.user_id'),
        '#placeholder'         => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
      ],

      'user_api_key' => [
        '#type'                => 'password',
        '#title'               => $this->t('CKAN User API key'),
        '#description'         => $this->t('The API key of the CKAN user which is used for all API interactions.'),
        '#description_display' => 'before',
        '#default_value'       => $currentConfig->get('connection.user_api_key'),
        '#placeholder'         => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
      ],

      'cache_duration' => [
        '#type'                => 'number',
        '#title'               => 'Cache duration (in minutes)',
        '#description'         => $this->t('How long information retrieved from the Dutch national dataportal will be held in the cache. Entering "0" will disable caching.'),
        '#description_display' => 'before',
        '#default_value'       => $currentConfig->get('connection.cache_duration') ?? 0,
        '#min'                 => 0,
      ],
    ];

    $form['metadata'] = [
      '#type'                => 'fieldset',
      '#title'               => $this->t('DCAT metadata'),
      '#description'         => $this->t('Settings related to how the DCAT metadata will be published and rendered.'),
      '#description_display' => 'before',

      'name_prefix' => [
        '#type'                => 'textfield',
        '#title'               => $this->t('Name prefix'),
        '#description'         => $this->t('The text that will be prefixed to the generated name of a DCAT object before it is published on the Dutch national dataportal. The prefix may not be longer than 10 characters and may only contain the lowercase letters a to z and a hyphen (-).'),
        '#description_display' => 'before',
        '#default_value'       => $currentConfig->get('metadata.name_prefix'),
        '#placeholder'         => 'e.g. xsp-',
        '#maxlength'           => 10,
        '#pattern'             => '[a-z\-]+',
      ],

      'link_pattern' => [
        '#type'                => 'textfield',
        '#title'               => $this->t('Link pattern'),
        '#description'         => $this->t('The pattern that instructs this portal how to generate a link to a DCAT object on the Dutch national dataportal.'),
        '#description_display' => 'before',
        '#default_value'       => $currentConfig->get('metadata.link_pattern'),
        '#placeholder'         => 'e.g. https://data.overheid.nl/dataset/',
      ],

      'source_catalog' => $this->createSourceCatalogInput($currentConfig),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $config = $this->config(XsDonlConnect::SETTINGS_KEY);

    $currentFeatureState = $config->get('enable');
    $newFeatureState     = 'checked' === $form_state->getValue('enable');

    $config
      ->set('enable', $newFeatureState)
      ->set('connection.endpoint', $form_state->getValue('endpoint'))
      ->set('connection.owner_org', $form_state->getValue('owner_org'))
      ->set('connection.user_id', $form_state->getValue('user_id'))
      ->set('connection.user_api_key', $form_state->getValue('user_api_key'))
      ->set('connection.cache_duration', $form_state->getValue('cache_duration'))
      ->set('metadata.name_prefix', $form_state->getValue('name_prefix'))
      ->set('metadata.link_pattern', $form_state->getValue('link_pattern'))
      ->set('metadata.source_catalog', $form_state->getValue('source_catalog'))
      ->save();

    if ($currentFeatureState !== $newFeatureState) {
      // Rebuild the route cache to ensure that Drupal is aware of which routes are made available via this module.
      // Additionally, the "datasets" cache tag is invalidated.
      $this->routeBuilder->rebuild();
      $this->cacheTagsInvalidator->invalidateTags(['datasets']);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [XsDonlConnect::SETTINGS_KEY];
  }

  /**
   * Create the render array for the source_catalog configuration value.
   *
   * @param Config $currentConfig The current stored configuration
   *
   * @return array<string, mixed> The Drupal render array
   */
  private function createSourceCatalogInput(Config $currentConfig): array
  {
    $sourceCatalogList = $this->listClient->list('donl:Catalog');
    $baseElements      = [
      '#title'               => $this->t('Source catalog'),
      '#description'         => $this->t('Assign the value that will be stored as the "source_catalog" attribute before the DCAT object will be published on the Dutch national dataportal. Only values from the "donl:Catalog" taxonomy are permitted.'),
      '#description_display' => 'before',
      '#default_value'       => $currentConfig->get('metadata.source_catalog'),
      '#required'            => FALSE,
    ];

    return $sourceCatalogList->hasData()
      ? array_merge($baseElements, [
        '#type'         => 'select',
        '#options'      => $this->listClient->formatter()->flatten($sourceCatalogList),
        '#sort_options' => TRUE,
        '#multiple'     => FALSE,
        '#empty_value'  => '',
      ])
      : array_merge($baseElements, [
        '#type'        => 'textfield',
        '#placeholder' => 'e.g. https://portals.xpertselect.nl/',
      ]);
  }
}
