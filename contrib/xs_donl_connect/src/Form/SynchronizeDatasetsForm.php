<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_donl_connect\Batch\SynchronizeAction;
use Drupal\xs_donl_connect\XsDonlConnect;
use Symfony\Component\DependencyInjection\ContainerInterface;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class SynchronizeDatasetsForm.
 *
 * Form for synchronizing datasets from this portal with Data.overheid.nl.
 */
final class SynchronizeDatasetsForm extends FormBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var CkanSdk $localCkanSdk */
    $localCkanSdk = $container->get('xs_ckan.ckan_sdk.admin');

    /** @var CkanSdk $donlCkanSdk */
    $donlCkanSdk = $container->get('xs_donl_connect.ckan.sdk');

    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    $xsDonlConnectConfig = $configFactory->get(XsDonlConnect::SETTINGS_KEY);
    $systemSiteConfig    = $configFactory->get('system.site');

    return new self(
      $localCkanSdk,
      $donlCkanSdk,
      trim($xsDonlConnectConfig->get('connection.user_id') ?? ''),
      $xsDonlConnectConfig->get('enable') ?? FALSE,
      $systemSiteConfig->get('name')
    );
  }

  /**
   * SynchronizeDatasetsForm constructor.
   *
   * @param CkanSdk $localCkanSdk           The CKAN SDK for communicating with CKAN of this portal
   * @param CkanSdk $donlCkanSdk            The CKAN SDK for communicating with CKAN of Data.overheid.nl
   * @param string  $donlUserId             The CKAN user id of the user that publishes datasets of this portal on Data.overheid.nl
   * @param bool    $synchronizationEnabled Whether synchronization is enabled
   * @param string  $siteName               The site name of this portal
   */
  public function __construct(private readonly CkanSdk $localCkanSdk, private readonly CkanSdk $donlCkanSdk,
                              private readonly string $donlUserId, private readonly bool $synchronizationEnabled,
                              private readonly string $siteName)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'xs_donl_connect_synchronize_datasets_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    try {
      $localDatasetCount = $this->localCkanSdk->datasets()->count(['q' => '*:*', 'include_private' => FALSE]);
    } catch (ClientException|ResponseException) {
      $localDatasetCount = $this->t('Unable to retrieve the number of public datasets on this portal.');
    }

    try {
      $donlDatasetCount = $this->donlCkanSdk->datasets()->count(
        ['q' => sprintf('creator_user_id:"%s"', $this->donlUserId), 'include_private' => FALSE]
      );
    } catch (ClientException|ResponseException) {
      $donlDatasetCount = $this->t('Unable to retrieve the number of datasets on Data.overheid.nl.');
    }

    return [
      'donl_table' => [
        '#type'   => 'table',
        '#header' => [
          ['colspan' => 2, 'data' => $this->t('Datasets')],
        ],
        '#rows'   => [
          [
            $this->t('@site (public)', ['@site' => $this->siteName]),
            $localDatasetCount,
          ],
          [
            'Data.overheid.nl',
            $donlDatasetCount,
          ],
        ],
      ],
      'synchronize_datasets' => [
        '#type'        => 'details',
        '#open'        => TRUE,
        '#title'       => $this->t('Synchronize datasets'),
        '#description' => $this->t('Press the button below to synchronize all datasets from @site with Data.overheid.nl.', [
          '@site' => $this->siteName,
        ]),
        'submit'       => [
          '#type'     => 'submit',
          '#value'    => $this->t('Synchronize'),
          '#disabled' => !$this->synchronizationEnabled,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    try {
      if (FALSE === $this->synchronizationEnabled) {
        $this->messenger()->addError('Synchronization with the Dutch national dataportal is disabled.');

        return;
      }

      $datasetsToPublish = $this->localCkanSdk->datasets()->all(['q' => '*:*', 'include_private' => FALSE]);
      $donlDatasets      = $this->donlCkanSdk->datasets()->all(
        ['q' => sprintf('creator_user_id:"%s"', $this->donlUserId), 'include_private' => FALSE]
      );

      $datasetsToPurge = $this->getDatasetIdentifiersToPurge(
        array_map(function(array $dataset): string { return $dataset['identifier']; }, $datasetsToPublish),
        array_map(function(array $dataset): string { return $dataset['identifier']; }, $donlDatasets)
      );

      $operations = array_merge(
        array_map(function(array $dataset) {
          return [
            '\Drupal\xs_donl_connect\Batch\SynchronizeDatasetsBatchProcessor::process',
            [$dataset['id'], SynchronizeAction::Publish],
          ];
        }, $datasetsToPublish),
        array_map(function(string $identifier) {
          return [
            '\Drupal\xs_donl_connect\Batch\SynchronizeDatasetsBatchProcessor::process',
            [$identifier, SynchronizeAction::Purge],
          ];
        }, $datasetsToPurge)
      );

      batch_set([
        'title'            => $this->t('Synchronizing datasets with the Dutch national dataportal'),
        'init_message'     => $this->t('Starting synchronizing datasets.'),
        'progress_message' => $this->t('Synchronized dataset @current/@total.'),
        'error_message'    => $this->t('An error occurred while synchronizing datasets.'),
        'finished'         => '\Drupal\xs_donl_connect\Batch\SynchronizeDatasetsBatchProcessor::finalize',
        'operations'       => $operations,
      ]);
    } catch (ClientException|ResponseException) {
      $this->messenger()->addError($this->t('Unable to get datasets in CKAN. Consult the Drupal logs for more details.'));
    }
  }

  /**
   * Get dataset identifiers that should be deleted from Data.overheid.nl.
   *
   * @param string[] $localDatasets The array of dataset identifiers of this portal
   * @param string[] $donlDatasets  The array of dataset identifiers on Data.overheid.nl of the user that publishes
   *                                datasets of this portal
   *
   * @return string[] The array of dataset identifiers that should be deleted from Data.overheid.nl
   */
  private function getDatasetIdentifiersToPurge(array $localDatasets, array $donlDatasets): array
  {
    $datasetIdentifiersToPurge = [];

    foreach ($donlDatasets as $identifier) {
      if (!in_array($identifier, $localDatasets)) {
        $datasetIdentifiersToPurge[] = $identifier;
      }
    }

    return $datasetIdentifiersToPurge;
  }
}
