<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect\Ckan;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\xs_donl_connect\XsDonlConnect;
use GuzzleHttp\Psr7\HttpFactory;
use XpertSelect\CkanSdk\HttpRequestService;

/**
 * Class HttpRequestServiceFactory.
 *
 * Factory class for creating HttpRequestService instances that can be used to interact with the Dutch national
 * dataportal.
 *
 * @see HttpRequestService
 */
final class HttpRequestServiceFactory
{
  /**
   * Creates a HttpRequestService instance using the Guzzle client provided by Drupal.
   *
   * @param ClientFactory          $guzzleFactory The factory for creating Guzzle client instances
   * @param ConfigFactoryInterface $configFactory The configuration factory holding the CKAN endpoint value
   *
   * @return HttpRequestService The created instance
   */
  public static function createService(ClientFactory $guzzleFactory,
                                       ConfigFactoryInterface $configFactory): HttpRequestService
  {
    $settings    = $configFactory->get(XsDonlConnect::SETTINGS_KEY);
    $httpFactory = new HttpFactory();

    $service = new HttpRequestService(
      $settings->get('connection.endpoint') ?? '',
      $guzzleFactory->fromOptions([
        'timeout' => 30,
        'headers' => ['User-Agent' => 'XS DONL Connect'],
      ]),
      $httpFactory,
      $httpFactory
    );

    if ($apiKey = $settings->get('connection.user_api_key')) {
      $service->setApiKey($apiKey);
    }

    return $service;
  }
}
