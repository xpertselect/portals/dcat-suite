<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\xs_ckan\CkanSdk;
use Psr\Log\LoggerInterface;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class DatasetPurger.
 *
 * A Drupal service for purging published datasets from the Dutch national dataportal.
 */
final readonly class DatasetPurger
{
  /**
   * Creates a new DatasetPurger instance.
   *
   * @param CkanSdk                       $donlSdk              The SDK for interacting with the API of the Dutch
   *                                                            national dataportal
   * @param ConfigFactoryInterface        $configFactory        The Drupal configuration factory
   * @param LoggerChannelFactoryInterface $loggerChannelFactory The factory for creating logging instances
   *
   * @return self The created instance
   */
  public static function create(CkanSdk $donlSdk, ConfigFactoryInterface $configFactory,
                                LoggerChannelFactoryInterface $loggerChannelFactory): self
  {
    return new self(
      $donlSdk,
      $configFactory->get(XsDonlConnect::SETTINGS_KEY)->get('connection.user_id') ?? '',
      $loggerChannelFactory->get(XsDonlConnect::LOG_CHANNEL)
    );
  }

  /**
   * DatasetPurger constructor.
   *
   * @param CkanSdk         $donlSdk    The SDK for interacting with the API of the Dutch national dataportal
   * @param string          $ckanUserId The ID of the CKAN user that owns the datasets on the Dutch national dataportal
   * @param LoggerInterface $logger     The logger to use
   */
  public function __construct(private CkanSdk $donlSdk, private string $ckanUserId,
                              private LoggerInterface $logger)
  {
  }

  /**
   * Determine if this service is in a healthy state.
   *
   * @return bool Whether the service is healthy
   */
  public function isHealthy(): bool
  {
    if (empty($this->ckanUserId)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Deletes a dataset with the given DCAT identifier from the Dutch national dataportal, assuming it exists there.
   *
   * @param null|string $identifier The identifier of the dataset
   *
   * @return bool Whether purging was successful
   */
  public function purgeDataset(?string $identifier): bool
  {
    if (is_null($identifier)) {
      return FALSE;
    }

    $dataset = $this->lookupDonlDataset($identifier);

    if (is_null($dataset)) {
      return FALSE;
    }

    try {
      $this->donlSdk->datasets()->delete($dataset['name']);

      return TRUE;
    } catch (ClientException|ResponseException $e) {
      $message = $e->getMessage() ?: 'n/a';

      if ($e instanceof ResponseException) {
        $message = $e->response->getPsrResponse()->getBody()->getContents();
      }

      $this->logger->error(sprintf(
        'Deleting dataset with identifier "%s" from the Dutch national dataportal failed; %s',
        $identifier,
        $message
      ));

      return FALSE;
    }
  }

  /**
   * Perform a lookup of a dataset on the Dutch national dataportal based on its DCAT identifier.
   *
   * @param string $identifier The DCAT identifier of the dataset
   *
   * @return null|array<string, mixed> The dataset on the Dutch national dataportal or null if it does not exist
   */
  private function lookupDonlDataset(string $identifier): ?array
  {
    try {
      $searchResult = $this->donlSdk->datasets()->search([
        'q' => sprintf('identifier:"%s" AND creator_user_id:"%s"', $identifier, $this->ckanUserId),
      ]);

      if (count($searchResult['results']) > 0) {
        return $searchResult['results'][0];
      }
    } catch (ClientException|ResponseException $e) {
      $this->logger->error('Unexpected response from the DONL API, skipping item; ' . $e->getMessage());
    }

    return NULL;
  }
}
