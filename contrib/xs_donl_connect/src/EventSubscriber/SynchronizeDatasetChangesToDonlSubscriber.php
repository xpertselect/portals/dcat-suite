<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect\EventSubscriber;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_donl_connect\DatasetPublisher;
use Drupal\xs_donl_connect\DatasetPurger;
use Drupal\xs_donl_connect\XsDonlConnect;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use XpertSelect\CkanSdk\Event\DatasetCreated;
use XpertSelect\CkanSdk\Event\DatasetDeleted;
use XpertSelect\CkanSdk\Event\DatasetUpdated;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class SynchronizeDatasetChangesToDonlSubscriber.
 *
 * Subscriber that listens to dataset events emitted from the default CKAN SDK instance and creates queue jobs to
 * synchronize the dataset mutations to the Dutch national dataportal.
 */
final class SynchronizeDatasetChangesToDonlSubscriber implements EventSubscriberInterface
{
  /**
   * Creates a new SynchronizeDatasetChangesToDonlSubscriber instance.
   *
   * @param CkanSdk                       $localSdk             The SDK for interacting with the local CKAN instance
   * @param DatasetPublisher              $datasetPublisher     The service for publishing datasets on the Dutch
   *                                                            national dataportal
   * @param DatasetPurger                 $datasetPurger        The service for removing datasets from the Dutch
   *                                                            national dataportal
   * @param ConfigFactoryInterface        $configFactory        The Drupal configuration factory
   * @param CacheTagsInvalidatorInterface $cacheTagsInvalidator The service for invalidating cache tags
   *
   * @return self The created instance
   */
  public static function create(CkanSdk $localSdk, DatasetPublisher $datasetPublisher, DatasetPurger $datasetPurger,
                                ConfigFactoryInterface $configFactory,
                                CacheTagsInvalidatorInterface $cacheTagsInvalidator): self
  {
    return new self(
      $localSdk,
      $datasetPublisher,
      $datasetPurger,
      $configFactory->get(XsDonlConnect::SETTINGS_KEY)->get('enable') ?? FALSE,
      $cacheTagsInvalidator
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [
      DatasetCreated::class => 'synchronizeDataset',
      DatasetUpdated::class => 'synchronizeDataset',
      DatasetDeleted::class => 'synchronizeDataset',
    ];
  }

  /**
   * SynchronizeDatasetChangesToDonlSubscriber constructor.
   *
   * @param CkanSdk                       $localSdk               The SDK for interacting with the local CKAN instance
   * @param DatasetPublisher              $datasetPublisher       The service for publishing datasets on the Dutch
   *                                                              national dataportal
   * @param DatasetPurger                 $datasetPurger          The service for removing datasets from the Dutch
   *                                                              national dataportal
   * @param bool                          $synchronizationEnabled Whether the synchronization feature is enabled
   * @param CacheTagsInvalidatorInterface $cacheTagsInvalidator   The service for invalidating cache tags
   */
  public function __construct(private readonly CkanSdk $localSdk, private readonly DatasetPublisher $datasetPublisher,
                              private readonly DatasetPurger $datasetPurger,
                              private readonly bool $synchronizationEnabled,
                              private readonly CacheTagsInvalidatorInterface $cacheTagsInvalidator)
  {
  }

  /**
   * Add the synchronization event to the queue for later processing. Only Dataset events emitted from the default
   * instance are processed.
   *
   * @param DatasetCreated|DatasetUpdated|DatasetDeleted $event The event to add to the queue
   */
  public function synchronizeDataset(DatasetCreated|DatasetUpdated|DatasetDeleted $event): void
  {
    if (!$this->shouldSynchronize($event)) {
      return;
    }

    if ($event instanceof DatasetCreated || $event instanceof DatasetUpdated) {
      $dataset = $this->loadLocalDataset($event->getNameOrId());

      if (is_null($dataset)) {
        return;
      }

      XsDonlConnect::datasetIsPrivate($dataset)
        ? $this->datasetPurger->purgeDataset($dataset['identifier'])
        : $this->datasetPublisher->publishDataset($dataset);
    }

    if ($event instanceof DatasetDeleted) {
      $this->datasetPurger->purgeDataset($event->getIdentifier());
    }

    $this->cacheTagsInvalidator->invalidateTags(['donl_dataset:' . $event->getNameOrId()]);
  }

  /**
   * Check whether the event should be processed.
   *
   * @param DatasetCreated|DatasetUpdated|DatasetDeleted $event The fired event
   *
   * @return bool Whether to start synchronizing the dataset
   */
  private function shouldSynchronize(DatasetCreated|DatasetUpdated|DatasetDeleted $event): bool
  {
    if (!$this->synchronizationEnabled) {
      return FALSE;
    }

    if (!$event->hasInstanceId() || !in_array($event->getInstanceId(), ['default', 'default.user', 'default.admin'])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Load a local dataset from the system.
   *
   * @param string $idOrName The ID or name attribute of the dataset
   *
   * @return null|array<string, mixed> The requested dataset or null if it could not be retrieved
   */
  private function loadLocalDataset(string $idOrName): ?array
  {
    try {
      return $this->localSdk->datasets()->get($idOrName);
    } catch (ClientException|ResponseException) {
      return NULL;
    }
  }
}
