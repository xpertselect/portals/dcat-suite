<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\xs_donl_connect\XsDonlConnect;
use Drupal\xs_general\DrupalUrlUtilities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class AlterViewDatasetSubscriber.
 *
 * Alters the render arrays of specific routes to attach the `xs_donl_connect/donl-lookup` library.
 */
final class AlterViewDatasetSubscriber implements EventSubscriberInterface
{
  use DrupalUrlUtilities;

  /**
   * The list of Drupal routes that may be altered by this subscriber.
   *
   * @var array<int, string>
   */
  public const APPLICABLE_ROUTES = [
    'xs_dataset.dataset.view',
  ];

  /**
   * Creates a new AlterViewDatasetSubscriber instance.
   *
   * @param ConfigFactoryInterface $configFactory The Drupal configuration factory
   *
   * @return self The created instance
   */
  public static function create(ConfigFactoryInterface $configFactory): self
  {
    return new self($configFactory->get(XsDonlConnect::SETTINGS_KEY)->get('enable') ?? FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [KernelEvents::VIEW => [['attachLookupLibrary', 50]]];
  }

  /**
   * AlterViewDatasetSubscriber constructor.
   *
   * @param bool $synchronizationEnabled Whether the synchronization to the Dutch national dataportal is enabled
   */
  public function __construct(private readonly bool $synchronizationEnabled)
  {
  }

  /**
   * Attach the `xs_donl_connect/donl-lookup` library to the render arrays of the applicable routes.
   *
   * @param ViewEvent $event The event to process
   */
  public function attachLookupLibrary(ViewEvent $event): void
  {
    if (!$this->synchronizationEnabled) {
      return;
    }

    if (!in_array($event->getRequest()->attributes->get('_route'), self::APPLICABLE_ROUTES)) {
      return;
    }

    $renderArray = $event->getControllerResult();

    if (!is_array($renderArray)) {
      return;
    }

    $ajaxCall = Url::fromRoute('xs_donl_connect.ajax.donl-lookup', ['dataset' => 'PLACEHOLDER']);

    $renderArray['#attached']['library'][]                                            = 'xs_donl_connect/donl-lookup';
    $renderArray['#attached']['drupalSettings']['xs_donl_connect']['donl_lookup_url'] = $this->drupalUrlAsString($ajaxCall);

    $event->setControllerResult($renderArray);
  }
}
