<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect\Controller;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_donl_connect\XsDonlConnect;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class DonlLookupController.
 *
 * Performs a lookup on the Dutch national dataportal to determine if a dataset is published there.
 */
final class DonlLookupController extends ControllerBase
{
  /**
   * The `sprintf` pattern to generate the cache ID.
   *
   * @var string
   */
  private const CACHE_PATTERN = 'xs_donl_connect:[dataset]=%s';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var CkanSdk $donlSdk */
    $donlSdk = $container->get('xs_donl_connect.ckan.sdk');

    /** @var CacheBackendInterface $cacheBackend */
    $cacheBackend = $container->get('cache.default');

    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');
    $config        = $configFactory->get(XsDonlConnect::SETTINGS_KEY);

    return new self(
      $donlSdk,
      $config->get('metadata.link_pattern') ?? '',
      $cacheBackend,
      intval($config->get('connection.cache_duration') ?? 0)
    );
  }

  /**
   * DonlLookupController constructor.
   *
   * @param CkanSdk               $donlSdk       The SDK for interacting with the Dutch national dataportal
   * @param string                $linkPattern   The pattern for creating a DONL reference
   * @param CacheBackendInterface $cacheBackend  The caching implementation
   * @param int                   $cacheDuration How long entries should be cached
   */
  public function __construct(private readonly CkanSdk $donlSdk, private readonly string $linkPattern,
                              private readonly CacheBackendInterface $cacheBackend, private readonly int $cacheDuration)
  {
  }

  /**
   * Perform a lookup on the Dutch national dataportal and return the information as a JSON response.
   *
   * @param array<string, mixed> $dataset The dataset to lookup
   *
   * @return JsonResponse The appropriate JSON response
   */
  public function lookup(array $dataset): JsonResponse
  {
    if (empty($this->linkPattern)) {
      return new JsonResponse(status: 501);
    }

    if (XsDonlConnect::datasetIsPrivate($dataset)) {
      return new JsonResponse(status: 404);
    }

    $cid = sprintf(self::CACHE_PATTERN, $dataset['name']);

    if (($cachedLink = $this->cacheBackend->get($cid)) && isset($cachedLink->data)) {
      return new JsonResponse(['location' => $cachedLink->data]);
    }

    try {
      $donlLocation = $this->performDonlLookup($dataset);

      if (is_null($donlLocation)) {
        return new JsonResponse(status: 404);
      }

      if ($this->cacheDuration > 0) {
        $this->cacheBackend->set($cid, $donlLocation, time() + ($this->cacheDuration * 60), [
          'xs_donl_connect',
          'donl_dataset:' . $dataset['id'],
          'donl_dataset:' . $dataset['name'],
        ]);
      }

      return new JsonResponse(['location' => $donlLocation]);
    } catch (ClientException|ResponseException) {
      return new JsonResponse(status: 501);
    }
  }

  /**
   * Lookup the given dataset on the Dutch national dataportal and return a link to that dataset if it exists.
   *
   * @param array<string, mixed> $dataset The dataset to lookup on the Dutch national dataportal
   *
   * @return null|string The `name` of the dataset on the Dutch national dataportal or null if it does not exist there
   *
   * @throws ResponseException Thrown on any error while communicating with the Dutch national dataportal
   * @throws ClientException   Thrown on any error while communicating with the Dutch national dataportal
   */
  private function performDonlLookup(array $dataset): ?string
  {
    $searchResults = $this->donlSdk->datasets()->search([
      'rows' => 1,
      'q'    => sprintf('identifier:"%s" AND title:"%s" AND authority:"%s"',
        $dataset['identifier'],
        $dataset['title'],
        $dataset['authority']
      ),
    ]);

    if (0 === count($searchResults['results'])) {
      return NULL;
    }

    return $this->linkPattern . $searchResults['results'][0]['name'];
  }
}
