<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\xs_donl_connect\XsDonlConnect;
use Symfony\Component\Routing\Route;

/**
 * Class FeatureToggledRoutes.
 *
 * Offers additional routes to the Drupal installation when the synchronization feature is enabled.
 */
final class FeatureToggledRoutes
{
  /**
   * Creates a FeatureToggledRoutes instance.
   *
   * @param ConfigFactoryInterface $configFactory The factory holding the xs_donl_connect module settings
   *
   * @return self The created instance
   */
  public static function create(ConfigFactoryInterface $configFactory): self
  {
    return new self(
      boolval($configFactory->get(XsDonlConnect::SETTINGS_KEY)->get('enable'))
    );
  }

  /**
   * FeatureToggledRoutes constructor.
   *
   * @param bool $synchronizationEnabled Whether the synchronization feature is enabled
   */
  public function __construct(private readonly bool $synchronizationEnabled)
  {
  }

  /**
   * Expose the appropriate routes when the synchronization feature is enabled.
   *
   * @return array<string, Route> The offered routes
   */
  public function getRoutes(): array
  {
    if (!$this->synchronizationEnabled) {
      return [];
    }

    return [
      'xs_donl_connect.ajax.donl-lookup' => new Route(
        path: '/ajax/donl-lookup/{dataset}',
        defaults: ['_controller' => '\Drupal\xs_donl_connect\Controller\DonlLookupController::lookup'],
        requirements: ['_permission' => 'access content'],
        options: ['parameters' => ['dataset' => ['type' => 'dataset']]],
        methods: 'GET'
      ),
    ];
  }
}
