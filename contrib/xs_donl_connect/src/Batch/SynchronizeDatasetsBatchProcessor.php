<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect\Batch;

use Drupal;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_donl_connect\DatasetPublisher;
use Drupal\xs_donl_connect\DatasetPurger;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class SynchronizeDatasetsBatchProcessor.
 *
 * The batch processor that publishes and purges datasets from this portal on and from Data.overheid.nl.
 */
final class SynchronizeDatasetsBatchProcessor
{
  use StringTranslationTrait;

  /**
   * Factory method for creating an instance of SynchronizeDatasetsBatchProcessor.
   *
   * @param CkanSdk                       $localCkanSdk         The CKAN SDK for communicating with CKAN of this portal
   * @param DatasetPublisher              $datasetPublisher     The dataset publisher that publishes datasets on Data.overheid.nl
   * @param DatasetPurger                 $datasetPurger        The dataset purger that purges datasets from Data.overheid.nl
   * @param MessengerInterface            $messenger            The messenger for displaying messages to the user
   * @param CacheTagsInvalidatorInterface $cacheTagsInvalidator The cache tags invalidator to invalidate cached links to Data.overheid.nl
   */
  public static function create(CkanSdk $localCkanSdk, DatasetPublisher $datasetPublisher, DatasetPurger $datasetPurger,
                                MessengerInterface $messenger, CacheTagsInvalidatorInterface $cacheTagsInvalidator): self
  {
    return new self($localCkanSdk, $datasetPublisher, $datasetPurger, $messenger, $cacheTagsInvalidator);
  }

  /**
   * Retrieve an instance of the SynchronizeDatasetsBatchProcessor from the Drupal container.
   *
   * @return SynchronizeDatasetsBatchProcessor The retrieved instance
   */
  public static function instance(): SynchronizeDatasetsBatchProcessor
  {
    return Drupal::service('xs_donl_connect.batch.synchronize-datasets-batch-processor');
  }

  /**
   * Publishes a dataset using the local identifier or purges a dataset using the global URI identifier.
   *
   * @param string            $identifier A local identifier when publishing or a global URI identifier when purging
   * @param SynchronizeAction $action     Publish or purge
   * @param array             $context    The Drupal batch context
   *
   * @see SynchronizeDatasetsBatchProcessor::processBatch()
   */
  public static function process(string $identifier, SynchronizeAction $action, array &$context): void
  {
    self::instance()->processBatch($identifier, $action, $context);
  }

  /**
   * Finalize the Data.overheid.nl synchronization datasets batch operation. Calls
   * SynchronizeDatasetsBatchProcessor::finalizeBatch internally.
   *
   * @param bool                $success    Whether the batch operation succeeded
   * @param array<int, mixed>   $results    The results of the individual operations in the batch
   * @param array<mixed, mixed> $operations The executed operations
   *
   * @see SynchronizeDatasetsBatchProcessor::finalizeBatch()
   */
  public static function finalize(bool $success, array $results, array $operations): void
  {
    self::instance()->finalizeBatch($success, $results, $operations);
  }

  /**
   * SynchronizeDatasetsBatchProcessor constructor.
   *
   * @param CkanSdk                       $localCkanSdk         The CKAN SDK for communicating with CKAN of this portal
   * @param DatasetPublisher              $datasetPublisher     The dataset publisher that publishes datasets on Data.overheid.nl
   * @param DatasetPurger                 $datasetPurger        The dataset purger that purges datasets from Data.overheid.nl
   * @param MessengerInterface            $messenger            The messenger for displaying messages to the user
   * @param CacheTagsInvalidatorInterface $cacheTagsInvalidator The cache tags invalidator to invalidate cached links to Data.overheid.nl
   */
  public function __construct(private readonly CkanSdk $localCkanSdk,
                              private readonly DatasetPublisher $datasetPublisher,
                              private readonly DatasetPurger $datasetPurger,
                              private readonly MessengerInterface $messenger,
                              private readonly CacheTagsInvalidatorInterface $cacheTagsInvalidator)
  {
  }

  /**
   * Publishes a dataset using the local identifier or purges a dataset using the global URI identifier.
   *
   * @param string            $identifier A local identifier when publishing or a global URI identifier when purging
   * @param SynchronizeAction $action     Publish or purge
   * @param array             $context    The Drupal batch context
   */
  public function processBatch(string $identifier, SynchronizeAction $action, array &$context): void
  {
    $result = ['action' => $action];

    if (SynchronizeAction::Publish === $action) {
      try {
        $localDataset = $this->localCkanSdk->datasets()->get($identifier);

        $context['message'] = $this->t('Publishing @dataset', ['@dataset' => $localDataset['identifier']]);

        $result['success'] = $this->datasetPublisher->publishDataset($localDataset);

        if ($result['success']) {
          $this->cacheTagsInvalidator->invalidateTags(['donl_dataset:' . $localDataset['id']]);
        }
      } catch (ClientException|ResponseException) {
        $result['success'] = FALSE;
      }
    } elseif (SynchronizeAction::Purge === $action) {
      $context['message'] = $this->t('Purging @dataset', ['@dataset' => $identifier]);

      $result['success'] = $this->datasetPurger->purgeDataset($identifier);
    }

    $context['results'][] = $result;
  }

  /**
   * Finalize the Data.overheid.nl synchronization datasets batch operation.
   *
   * @param bool                $success    Whether the batch operation succeeded
   * @param array<int, mixed>   $results    The results of the individual operations in the batch
   * @param array<mixed, mixed> $operations The executed operations
   */
  public function finalizeBatch(bool $success, array $results, array $operations): void
  {
    $errorCount       = 0;
    $publishedCount   = 0;
    $purgedCount      = 0;

    foreach ($results as $result) {
      if (!array_key_exists('action', $result)
        || !array_key_exists('success', $result)
        || FALSE === $result['success']) {
        ++$errorCount;
      } elseif (SynchronizeAction::Publish === $result['action']) {
        ++$publishedCount;
      } elseif (SynchronizeAction::Purge === $result['action']) {
        ++$purgedCount;
      }
    }

    if (!$success || $errorCount > 0) {
      $this->messenger->addError(
        $this->formatPlural(
          $errorCount,
          'There was @count error reported while synchronizing datasets. Consult the Drupal logs for more details.',
          'There were @count errors reported while synchronizing datasets. Consult the Drupal logs for more details.',
          ['@count' => $errorCount]
        )
      );
    }

    $this->messenger->addMessage($this->t('Published: @published, Purged: @purged', [
      '@published' => $publishedCount,
      '@purged'    => $purgedCount,
    ]));
  }
}
