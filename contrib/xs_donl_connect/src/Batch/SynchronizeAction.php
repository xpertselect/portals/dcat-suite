<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_donl_connect\Batch;

/**
 * Enum SynchronizeAction.
 *
 * All possible Data.overheid.nl synchronisation actions.
 */
enum SynchronizeAction
{
  case Publish;
  case Purge;
}
