<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_membership\Unit;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_membership\Ckan\CkanUserResolver;
use Drupal\xs_membership\MembershipFormInjector;
use Drupal\xs_membership\MembershipService;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_membership\TestCase;

/**
 * @internal
 */
final class MembershipFormInjectorTest extends TestCase
{
  public function testInjectDoesNothingWhenUserHasNoPermission(): void
  {
    $injector = new MembershipFormInjector(
      M::mock(AccountProxyInterface::class, function(MI $mock) {
        $mock->shouldReceive('hasPermission')
          ->with('change user memberships')
          ->andReturnFalse();
      }),
      M::mock(CkanUserResolver::class),
      M::mock(MembershipService::class),
      M::mock(CkanSdk::class)
    );

    $form = ['foo' => 'bar'];

    $injector->inject($form);

    Assert::assertArrayNotHasKey('memberships_container', $form);
  }

  public function testInjectDoesNothingWhenUserCannotBeResolved(): void
  {
    $injector = new MembershipFormInjector(
      M::mock(AccountProxyInterface::class, function(MI $mock) {
        $mock->shouldReceive('hasPermission')
          ->with('change user memberships')
          ->andReturnTrue();
      }),
      M::mock(CkanUserResolver::class, function(MI $mock) {
        $mock->shouldReceive('loadForUserInRoute')
          ->andReturnNull();
      }),
      M::mock(MembershipService::class),
      M::mock(CkanSdk::class)
    );

    $form = ['foo' => 'bar'];

    $injector->inject($form);

    Assert::assertArrayNotHasKey('memberships_container', $form);
  }
}
