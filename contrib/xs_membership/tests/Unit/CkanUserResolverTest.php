<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_membership\Unit;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_ckan\Repository\UserRepository;
use Drupal\xs_membership\Ckan\CkanUserResolver;
use Drupal\xs_membership\User;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_membership\TestCase;
use Throwable;

/**
 * @internal
 */
final class CkanUserResolverTest extends TestCase
{
  public function setUp(): void
  {
    parent::setUp();

    CkanUserResolver::resetResolverCache();
  }

  /**
   * @dataProvider ckanSdkExceptionDataset
   */
  public function testLoadForUserReturnsNullOnAnyException(Throwable $t): void
  {
    $ckanSdk = M::mock(CkanSdk::class, function(MI $mock) use ($t) {
      $userRepository = M::mock(UserRepository::class, function(MI $mock) use ($t) {
        $mock->shouldReceive('getOrCreate')->andThrow($t);
      });

      $mock->shouldReceive('users')->andReturn($userRepository);
    });

    $resolver = new CkanUserResolver(
      $ckanSdk,
      M::mock(AccountProxyInterface::class),
      M::mock(RouteMatchInterface::class)
    );

    Assert::assertNull($resolver->loadForUser(M::mock(UserInterface::class, function(MI $mock) {
      $mock->shouldReceive('id')->andReturn(1);
    })));
  }

  public function testLoadForUserReturnsUser(): void
  {
    $ckanSdk = M::mock(CkanSdk::class, function(MI $mock) {
      $userRepository = M::mock(UserRepository::class, function(MI $mock) {
        $mock->shouldReceive('getOrCreate')->with(User::NAME_PREFIX . 1)->andReturn([
          'foo' => 'bar',
        ]);
      });

      $mock->shouldReceive('users')->andReturn($userRepository);
    });

    $resolver = new CkanUserResolver(
      $ckanSdk,
      M::mock(AccountProxyInterface::class),
      M::mock(RouteMatchInterface::class)
    );

    Assert::assertIsArray($resolver->loadForUser(M::mock(UserInterface::class, function(MI $mock) {
      $mock->shouldReceive('id')->andReturn(1);
    })));
  }

  public function testLoadForUserInRouteReturnsNullWhenUserIsNotInRoute(): void
  {
    $resolver = new CkanUserResolver(
      M::mock(CkanSdk::class),
      M::mock(AccountProxyInterface::class),
      M::mock(RouteMatchInterface::class, function(MI $mock) {
        $mock->shouldReceive('getParameter')->with('user')->andReturnNull();
      })
    );

    Assert::assertNull($resolver->loadForUserInRoute());
  }

  /**
   * @dataProvider ckanSdkExceptionDataset
   */
  public function testLoadForUserInRouteReturnsNullOnAnyException(Throwable $t): void
  {
    $resolver = new CkanUserResolver(
      M::mock(CkanSdk::class, function(MI $mock) use ($t) {
        $userRepository = M::mock(UserRepository::class, function(MI $mock) use ($t) {
          $mock->shouldReceive('getOrCreate')->andThrow($t);
        });

        $mock->shouldReceive('users')->andReturn($userRepository);
      }),
      M::mock(AccountProxyInterface::class),
      M::mock(RouteMatchInterface::class, function(MI $mock) {
        $mock->shouldReceive('getParameter')
          ->with('user')
          ->andReturn(M::mock(UserInterface::class, function(MI $mock) {
            $mock->shouldReceive('id')->andReturn(1);
          }));
      })
    );

    Assert::assertNull($resolver->loadForUserInRoute());
  }

  public function testLoadForUserInRouteReturnsUser(): void
  {
    $resolver = new CkanUserResolver(
      M::mock(CkanSdk::class, function(MI $mock) {
        $userRepository = M::mock(UserRepository::class, function(MI $mock) {
          $mock->shouldReceive('getOrCreate')->with(User::NAME_PREFIX . 1)->andReturn([
            'foo' => 'bar',
          ]);
        });

        $mock->shouldReceive('users')->andReturn($userRepository);
      }),
      M::mock(AccountProxyInterface::class),
      M::mock(RouteMatchInterface::class, function(MI $mock) {
        $mock->shouldReceive('getParameter')
          ->with('user')
          ->andReturn(M::mock(UserInterface::class, function(MI $mock) {
            $mock->shouldReceive('id')->andReturn(1);
          }));
      })
    );

    Assert::assertIsArray($resolver->loadForUserInRoute());
  }
}
