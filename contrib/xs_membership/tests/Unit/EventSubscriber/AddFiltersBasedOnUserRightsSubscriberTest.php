<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_membership\Unit\EventSubscriber;

use Drupal\xs_membership\EventSubscriber\AddFiltersBasedOnUserRightsSubscriber;
use Drupal\xs_membership\MembershipService;
use Drupal\xs_searchable_content\Event\PreparingSuggestQuery;
use Drupal\xs_searchable_content\Services\Search\SuggestProfile;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_membership\TestCase;

/**
 * @internal
 */
final class AddFiltersBasedOnUserRightsSubscriberTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    $membershipService = M::mock(MembershipService::class);

    Assert::assertInstanceOf(
      AddFiltersBasedOnUserRightsSubscriber::class,
      new AddFiltersBasedOnUserRightsSubscriber($membershipService)
    );
  }

  public function testSubscriberListensToEvents(): void
  {
    Assert::assertNotEmpty(AddFiltersBasedOnUserRightsSubscriber::getSubscribedEvents());
  }

  public function testInstanceCanCreateFilterWithoutUserHavingRights(): void
  {
    $membershipService = M::mock(MembershipService::class, function(MI $mock) {
      $mock->shouldReceive('getMembershipsForUser')
        ->with(NULL, 'read', 'name')
        ->andReturn([]);
    });

    $event                = M::mock(PreparingSuggestQuery::class);
    $event->searchProfile = M::mock(SuggestProfile::class, function(MI $mock) {
      $mock->shouldReceive('addFilters')
        ->with(['(visibility:"public")']);
    });

    $subscriber = new AddFiltersBasedOnUserRightsSubscriber($membershipService);

    $subscriber->addVisibilityFiltersBasedOnRights($event);

    Assert::assertInstanceOf(
      AddFiltersBasedOnUserRightsSubscriber::class,
      $subscriber
    );
  }

  public function testInstanceCanCreateFilterWithUserHavingRights(): void
  {
    $membershipService = M::mock(MembershipService::class, function(MI $mock) {
      $mock->shouldReceive('getMembershipsForUser')
        ->with(NULL, 'read', 'name')
        ->andReturn([
          'b-s-n-bestuursondersteuning' => 'rights',
          'b-s-n-concerncontrol'        => 'rights',
        ]);
    });

    $event                = M::mock(PreparingSuggestQuery::class);
    $event->searchProfile = M::mock(SuggestProfile::class, function(MI $mock) {
      $mock->shouldReceive('addFilters')
        ->with(['(visibility:"public" OR (visibility:"private" AND organization:("b-s-n-bestuursondersteuning" OR "b-s-n-concerncontrol")))']);
    });

    $subscriber = new AddFiltersBasedOnUserRightsSubscriber($membershipService);

    $subscriber->addVisibilityFiltersBasedOnRights($event);

    Assert::assertInstanceOf(
      AddFiltersBasedOnUserRightsSubscriber::class,
      $subscriber
    );
  }
}
