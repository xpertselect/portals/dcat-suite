<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_membership\Unit;

use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_ckan\Repository\OrganizationRepository;
use Drupal\xs_membership\MaintainerTaxonomyService;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_membership\TestCase;
use Throwable;

/**
 * @internal
 */
final class MaintainerTaxonomyServiceTest extends TestCase
{
  /**
   * @dataProvider ckanSdkExceptionDataset
   */
  public function testAsDcatListReturnsEmptyArrayOnAnyException(Throwable $t): void
  {
    $service  = new MaintainerTaxonomyService(M::mock(CkanSdk::class, function(MI $mock) use ($t) {
      $organizations = M::mock(OrganizationRepository::class, function(MI $mock) use ($t) {
        $mock->shouldReceive('list')->andThrow($t);
      });

      $mock->shouldReceive('organizations')->andReturn($organizations);
    }));
    $taxonomy = $service->asDcatList();

    Assert::assertIsArray($taxonomy);
    Assert::assertCount(0, $taxonomy);
  }

  public function testAsDcatListCorrectlyTransformsCkanOrganizations(): void
  {
    $service  = new MaintainerTaxonomyService(M::mock(CkanSdk::class, function(MI $mock) {
      $organizations = M::mock(OrganizationRepository::class, function(MI $mock) {
        $mock->shouldReceive('list')->andReturn([
          ['name' => 'organization-a', 'title' => 'Organization A'],
          ['name' => 'organization-b', 'title' => 'Organization B'],
        ]);
      });

      $mock->shouldReceive('organizations')->andReturn($organizations);
    }));
    $taxonomy = $service->asDcatList();

    Assert::assertIsArray($taxonomy);
    Assert::assertNotEmpty($taxonomy);

    foreach ($taxonomy as $key => $entry) {
      Assert::assertIsString($key);
      Assert::assertArrayHasKey('labels', $entry);

      Assert::assertArrayHasKey('nl-NL', $entry['labels']);
      Assert::assertIsString($entry['labels']['nl-NL']);

      Assert::assertArrayHasKey('en-US', $entry['labels']);
      Assert::assertIsString($entry['labels']['en-US']);
    }
  }
}
