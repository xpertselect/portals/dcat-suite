<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_membership;

use Mockery as M;
use PHPUnit\Framework\TestCase as BaseTestCase;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;
use XpertSelect\PsrTools\PsrResponse;

/**
 * @internal
 */
abstract class TestCase extends BaseTestCase
{
  public static function ckanSdkExceptionDataset(): array
  {
    return [
      ['ClientException'   => new ClientException()],
      ['ResponseException' => new ResponseException(M::mock(PsrResponse::class))],
    ];
  }
}
