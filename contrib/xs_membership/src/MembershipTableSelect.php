<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\xs_ckan\CkanSdk;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Trait MembershipTableSelect.
 *
 * A trait that offers functionality to create table select Drupal form elements to manage the membership related data.
 */
trait MembershipTableSelect
{
  use StringTranslationTrait;

  /**
   * Creates a Drupal table select render array holding a combination of all the CKAN organizations and the roles a user
   * can hold in these organizations.
   *
   * @param CkanSdk               $ckanSdk      The SDK for interacting with CKAN
   * @param array<string, string> $defaultRoles The roles that should be preselected
   *
   * @return array<string, mixed> The Drupal render array
   */
  private function createTableSelect(CkanSdk $ckanSdk, array $defaultRoles = []): array
  {
    try {
      $options = [];

      foreach ($ckanSdk->organizations()->list() as $organization) {
        $options[$organization['id']] = [
          'organization' => $organization['display_name'],
          'role'         => [
            'data' => [
              '#type'    => 'select',
              '#options' => array_map(fn ($option) => $this->t($option), User::ROLE_MAP),
              '#name'    => sprintf('role[%s]', $organization['id']),
              '#value'   => $defaultRoles[$organization['id']] ?? NULL,
            ],
          ],
        ];
      }

      $defaults = [];

      foreach ($options as $organization => $option) {
        $defaults[$organization] = !empty($option['role']['data']['#value']);
      }

      return [
        '#type'          => 'tableselect',
        '#header'        => ['organization' => $this->t('Organization'), 'role' => $this->t('Role')],
        '#options'       => $options,
        '#empty'         => $this->t('No organizations found.'),
        '#default_value' => $defaults,
      ];
    } catch (ClientException|ResponseException) {
      return [];
    }
  }
}
