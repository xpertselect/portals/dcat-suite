<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\Entity\User;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_membership\Ckan\CkanUserResolver;
use Drupal\xs_membership\User as CkanUser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class MembershipsBlock.
 *
 * A Drupal block that renders the memberships of a Drupal user in a table.
 *
 * @Block(
 *   id = "xs_membership_user_memberships_block",
 *   admin_label = @Translation("Memberships"),
 * )
 */
final class MembershipsBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id,
                                $plugin_definition): MembershipsBlock
  {
    /** @var AccountProxyInterface $currentUser */
    $currentUser = $container->get('current_user');

    /** @var RouteMatchInterface $routeMatch */
    $routeMatch = $container->get('current_route_match');

    /** @var CkanSdk $ckanSdk */
    $ckanSdk = $container->get('xs_ckan.ckan_sdk.default');

    /** @var CkanUserResolver $ckanUserResolver */
    $ckanUserResolver = $container->get('xs_membership.ckan_user_resolver');

    return new MembershipsBlock(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $currentUser,
      $routeMatch,
      $ckanSdk,
      $ckanUserResolver
    );
  }

  /**
   * MembershipsBlock constructor.
   *
   * @param array<string, mixed>  $configuration     A configuration array containing information about the plugin
   * @param string                $plugin_id         The plugin_id for the plugin instance
   * @param mixed                 $plugin_definition The plugin implementation definition
   * @param AccountProxyInterface $currentUser       The current Drupal user
   * @param RouteMatchInterface   $routeMatch        The service for reasoning about the routes
   * @param CkanSdk               $ckanSdk           The SDK for interacting with CKAN
   * @param CkanUserResolver      $ckanUserResolver  The service for resolving CKAN users related to Drupal users
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              private readonly AccountProxyInterface $currentUser,
                              private readonly RouteMatchInterface $routeMatch, private readonly CkanSdk $ckanSdk,
                              private readonly CkanUserResolver $ckanUserResolver)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    $membershipRows = $this->getMembershipsForUser();

    return [
      '#type'       => 'fieldset',
      '#title'      => $this->t('Memberships (@count)', ['@count' => count($membershipRows)]),
      '#cache'      => ['max-age' => 0],
      '#attributes' => ['class' => ['memberships-block']],

      'organization_table' => [
        '#type'       => 'table',
        '#header'     => ['organization' => $this->t('Organization'), 'role' => $this->t('Role')],
        '#rows'       => $membershipRows,
        '#empty'      => $this->t('No memberships found.'),
        '#attributes' => ['class' => ['table', 'table-hover']],
        '#prefix'     => '<div class="table-responsive">',
        '#suffix'     => '</div>',
      ],
    ];
  }

  /**
   * Retrieve the CKAN membership entries as Drupal table rows.
   *
   * @return array<int, array<int, string|TranslatableMarkup>> The CKAN membership rows
   */
  private function getMembershipsForUser(): array
  {
    $drupalUser = $this->routeMatch->getParameter('user') ?? User::load($this->currentUser->id());

    if (is_null($drupalUser)) {
      return [];
    }

    $ckanUser = $this->ckanUserResolver->loadForUser($drupalUser);

    if (is_null($ckanUser)) {
      return [];
    }

    try {
      return array_map(function(array $organization) {
        return [$organization['display_name'], $this->t(CkanUser::ROLE_MAP[$organization['capacity']])];
      }, $this->ckanSdk->organizations()->listForUser($ckanUser['id']));
    } catch (ClientException|ResponseException) {
      return [];
    }
  }
}
