<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\xs_ckan\CkanSdk;
use Symfony\Component\Routing\Route;
use XpertSelect\CkanSdk\Repository\OrganizationRepository;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class OrganizationConverter.
 *
 * Converts {organization} arguments into CKAN organization objects.
 */
final class OrganizationConverter implements ParamConverterInterface
{
  /**
   * A in-memory cache of the resolved CKAN organizations.
   *
   * @var array<string, array<string, mixed>>
   */
  private static array $CACHE = [];

  /**
   * The CKAN repository for retrieving organizations.
   */
  private readonly OrganizationRepository $organizationRepository;

  /**
   * OrganizationConverter constructor.
   *
   * @param CkanSdk $ckanSdk The SDK for interacting with CKAN
   */
  public function __construct(CkanSdk $ckanSdk)
  {
    $this->organizationRepository = $ckanSdk->organizations();
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults): ?array
  {
    if (!is_string($value)) {
      return NULL;
    }

    if (array_key_exists($value, self::$CACHE)) {
      return self::$CACHE[$value];
    }

    try {
      $organization = $this->organizationRepository->get($value);

      self::$CACHE[$value] = $organization;

      return $organization;
    } catch (ClientException|ResponseException) {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool
  {
    return !empty($definition['type']) && 'organization' === $definition['type'];
  }
}
