<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership;

/**
 * Class User.
 *
 * Provides several constants related to users stored in the connected CKAN instance.
 */
final class User
{
  /**
   * The string to prefix to all created CKAN users.
   *
   * @var string
   */
  public const NAME_PREFIX = 'xs_drupal_user_';

  /**
   * The translation map where the keys are CKAN user roles and the values are English labels of that role.
   *
   * @var array<string, string>
   */
  public const ROLE_MAP = [
    'admin'  => 'Maintainer',
    'member' => 'Member',
  ];
}
