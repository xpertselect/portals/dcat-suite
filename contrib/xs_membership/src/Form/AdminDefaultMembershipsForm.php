<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_membership\MembershipTableSelect;
use Drupal\xs_membership\XsMembership;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdminDefaultMembershipsForm.
 *
 * A Drupal administration form to configure the default memberships that should be given to newly created Drupal users.
 */
final class AdminDefaultMembershipsForm extends ConfigFormBase
{
  use MembershipTableSelect;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): AdminDefaultMembershipsForm
  {
    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    /** @var CkanSdk $ckanSdk */
    $ckanSdk = $container->get('xs_ckan.ckan_sdk.default');

    return new AdminDefaultMembershipsForm($configFactory, $ckanSdk);
  }

  /**
   * AdminDefaultMembershipsForm constructor.
   *
   * @param ConfigFactoryInterface $config_factory The factory for configuration objects
   * @param CkanSdk                $ckanSdk        The SDK for interacting with CKAN
   */
  public function __construct(ConfigFactoryInterface $config_factory, protected readonly CkanSdk $ckanSdk)
  {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'xs_membership_default_memberships_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $config = $this->config(XsMembership::MEMBERSHIP_SETTINGS_KEY);

    $form['memberships_container'] = [
      '#type'                => 'fieldset',
      '#title'               => $this->t('Default memberships'),
      '#description'         => $this->t('Configure which memberships are automatically assigned when a new user is created. Note: Changing these does not affect the memberships of existing users.'),
      '#description_display' => 'before',

      'role'        => ['#type' => 'value'],
      'memberships' => $this->createTableSelect($this->ckanSdk, $config->get('default_roles') ?? []),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $requestRoles  = $this->getRequest()->get('role', []);
    $organizations = $form_state->getValue('memberships');
    $roles         = array_filter(array_map(fn ($organization) => $requestRoles[$organization] ?? NULL, $organizations));

    $this->config(XsMembership::MEMBERSHIP_SETTINGS_KEY)
      ->set('default_organizations', $organizations)
      ->set('default_roles', $roles)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [XsMembership::MEMBERSHIP_SETTINGS_KEY];
  }
}
