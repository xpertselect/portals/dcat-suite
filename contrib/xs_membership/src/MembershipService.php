<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\user\UserInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_membership\Ckan\CkanUserResolver;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class MembershipService.
 *
 * A Drupal service for managing the CKAN memberships (and thus permissions) of a Drupal user and its related CKAN user.
 */
final class MembershipService
{
  private readonly LoggerChannelInterface $logger;

  /**
   * MembershipService constructor.
   *
   * @param CkanSdk                       $ckanSdk          The SDK for interacting with the JSON API of a CKAN instance
   * @param CkanUserResolver              $ckanUserResolver The service for loading the CKAN user related to a Drupal user
   * @param LoggerChannelFactoryInterface $loggerFactory    The logging factory
   */
  public function __construct(private readonly CkanSdk $ckanSdk, private readonly CkanUserResolver $ckanUserResolver,
                              LoggerChannelFactoryInterface $loggerFactory)
  {
    $this->logger = $loggerFactory->get(XsMembership::LOG_CHANNEL);
  }

  /**
   * Retrieve the memberships of the given CKAN user.
   *
   * @param null|string $userId     The CKAN user ID
   * @param string      $permission The minimum permission that the user should have for an organization
   * @param string      $keyField   The field to use as the array key, defaults to 'id'
   * @param string      $labelField The field to use as the array value, defaults to 'capacity'
   *
   * @return array<string, string> The memberships in the form of {organization ID} => {label}
   */
  public function getMembershipsForUser(?string $userId = NULL, string $permission = 'read',
                                        string $keyField = 'id', string $labelField = 'capacity'): array
  {
    if (is_null($userId)) {
      $ckanUser = $this->ckanUserResolver->loadForCurrentUser();

      if (is_null($ckanUser)) {
        return [];
      }

      $userId = $ckanUser['id'];
    }

    try {
      $memberships = [];

      foreach ($this->ckanSdk->organizations()->listForUser($userId, $permission) as $membership) {
        $memberships[strval($membership[$keyField])] = strval($membership[$labelField]);
      }

      return $memberships;
    } catch (ClientException|ResponseException) {
      $this->logger->error('Failed to retrieve CKAN memberships for CKAN user @user', ['@user' => $userId]);

      return [];
    }
  }

  /**
   * Replace the memberships of the CKAN user connected to the given Drupal user with a new list of memberships.
   *
   * @param UserInterface         $drupalUser  The Drupal user to manage the memberships for
   * @param array<string, string> $memberships The memberships to register in the form of `{organization ID} => {role}`
   */
  public function assignMembershipsToUser(UserInterface $drupalUser, array $memberships): void
  {
    $ckanUser = $this->ckanUserResolver->loadForUser($drupalUser);

    if (is_null($ckanUser)) {
      return;
    }

    $this->removeUnspecifiedMemberships($ckanUser, $memberships);
    $this->registerNewMemberships($ckanUser, $memberships);
  }

  /**
   * Remove the user from the specified organization.
   *
   * @param string $organizationId The ID of the CKAN organization
   * @param string $ckanUserId     The ID of the CKAN user
   */
  public function removeMembership(string $organizationId, string $ckanUserId): void
  {
    try {
      $this->ckanSdk->organizationMemberships()->deleteUser($organizationId, $ckanUserId);
    } catch (ClientException|ResponseException) {
      $this->logger->error('Failed to delete membership of organization @organization for CKAN user @user', [
        '@organization' => $organizationId,
        '@user'         => $ckanUserId,
      ]);
    }
  }

  /**
   * Remove all current memberships that are not included in the list of memberships to set for the CKAN user.
   *
   * @param array<string, mixed>  $ckanUser    The CKAN user object
   * @param array<string, string> $memberships The memberships to register in the form of `{organization ID} => {role}`
   */
  private function removeUnspecifiedMemberships(array $ckanUser, array $memberships): void
  {
    try {
      $currentMemberships = $this->ckanSdk->organizations()->listForUser($ckanUser['id']);

      foreach ($currentMemberships as $membership) {
        if (!array_key_exists($membership['id'], $memberships)) {
          $this->removeMembership($membership['id'], $ckanUser['id']);
        }
      }
    } catch (ClientException|ResponseException) {
      $this->logger->error('Failed to determine the active memberships of CKAN user @user', [
        '@user' => $ckanUser['id'],
      ]);
    }
  }

  /**
   * Register new memberships for the CKAN user.
   *
   * @param array<string, mixed>  $ckanUser    The CKAN user object
   * @param array<string, string> $memberships The memberships to register in the form of `{organization ID} => {role}`
   */
  private function registerNewMemberships(array $ckanUser, array $memberships = []): void
  {
    foreach ($memberships as $organizationId => $role) {
      try {
        $this->ckanSdk->organizationMemberships()->addUser($organizationId, $ckanUser['id'], $role);
      } catch (ClientException|ResponseException) {
        $this->logger->error('Failed to assign role @role to CKAN organization @organization for CKAN user @user', [
          '@role'         => $role,
          '@organization' => $organizationId,
          '@user'         => $ckanUser['id'],
        ]);
      }
    }
  }
}
