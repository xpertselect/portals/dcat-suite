<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_membership\Ckan\CkanUserResolver;

/**
 * Class MembershipFormInjector.
 *
 * A Drupal service for injecting membership related form elements into a Drupal form.
 */
final class MembershipFormInjector
{
  use MembershipTableSelect;

  /**
   * MembershipFormInjector constructor.
   *
   * @param AccountProxyInterface $currentUser       The current Drupal user
   * @param CkanUserResolver      $ckanUserResolver  The Drupal to CKAN user resolver
   * @param MembershipService     $membershipService The service for managing memberships
   * @param CkanSdk               $ckanSdk           The SDK for interacting with CKAN
   */
  public function __construct(private readonly AccountProxyInterface $currentUser,
                              private readonly CkanUserResolver $ckanUserResolver,
                              private readonly MembershipService $membershipService,
                              private readonly CkanSdk $ckanSdk)
  {
  }

  /**
   * Inject the membership form elements into the given Drupal form construct.
   *
   * @param array<string, mixed> $form The Drupal form being created
   *
   * @see xs_membership_submit_user_memberships()
   */
  public function inject(array &$form): void
  {
    if (!$this->currentUser->hasPermission('change user memberships')) {
      return;
    }

    $user = $this->ckanUserResolver->loadForUserInRoute();

    if (is_null($user)) {
      return;
    }

    $form['memberships_container'] = [
      '#type'                => 'fieldset',
      '#title'               => $this->t('Memberships'),
      '#description'         => $this->t('Configure the memberships of this user.'),
      '#description_display' => 'before',

      'role'        => ['#type' => 'value'],
      'memberships' => $this->createTableSelect(
        $this->ckanSdk,
        $this->membershipService->getMembershipsForUser($user['id'])
      ),
    ];

    $form['actions']['submit']['#submit'][] = 'xs_membership_submit_user_memberships';
  }
}
