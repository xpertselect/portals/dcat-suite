<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership;

use Drupal\xs_ckan\CkanSdk;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class MaintainerTaxonomyService.
 *
 * A Drupal service that transforms the CKAN organizations into a representation supported by `xs_lists`.
 */
final class MaintainerTaxonomyService
{
  /**
   * MaintainerTaxonomyService constructor.
   *
   * @param CkanSdk $ckanSdk The SDK for interacting with CKAN
   */
  public function __construct(private readonly CkanSdk $ckanSdk)
  {
  }

  /**
   * Retrieve all the CKAN organizations and transform them into an array representation supported by `xs_lists`.
   *
   * @return array<string, array{labels: array{nl-NL: string, en-US: string}}> The formatted CKAN organizations
   */
  public function asDcatList(): array
  {
    try {
      $maintainers = [];

      foreach ($this->ckanSdk->organizations()->list() as $organization) {
        $maintainers[strval($organization['name'])] = [
          'labels' => [
            'nl-NL' => strval($organization['title']),
            'en-US' => strval($organization['title']),
          ],
        ];
      }

      return $maintainers;
    } catch (ClientException|ResponseException) {
      return [];
    }
  }
}
