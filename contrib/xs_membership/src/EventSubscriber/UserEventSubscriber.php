<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_membership\XsMembership;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use XpertSelect\CkanSdk\Event\UserCreated;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class UserEventSubscriber.
 *
 * A Drupal event subscriber that listens to events pertaining to CKAN users.
 */
final class UserEventSubscriber implements EventSubscriberInterface
{
  /**
   * Create a new UserEventSubscriber instance with the help of the given factories.
   *
   * @param CkanSdk                       $ckanSdk              The SDK for interacting with CKAN
   * @param ConfigFactoryInterface        $configFactory        The factory holding the Drupal configuration
   * @param LoggerChannelFactoryInterface $loggerChannelFactory The factory for creating logging implementations
   *
   * @return UserEventSubscriber The created instance
   */
  public static function create(CkanSdk $ckanSdk, ConfigFactoryInterface $configFactory,
                                LoggerChannelFactoryInterface $loggerChannelFactory): UserEventSubscriber
  {
    return new UserEventSubscriber(
      $ckanSdk,
      $configFactory->get(XsMembership::MEMBERSHIP_SETTINGS_KEY),
      $loggerChannelFactory->get(XsMembership::LOG_CHANNEL)
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [
      UserCreated::class => 'assignDefaultMemberships',
    ];
  }

  /**
   * UserEventSubscriber constructor.
   *
   * @param CkanSdk                $ckanSdk          The SDK for interacting with CKAN
   * @param ImmutableConfig        $membershipConfig The CKAN membership configuration
   * @param LoggerChannelInterface $logger           The logging instance to use
   */
  public function __construct(private readonly CkanSdk $ckanSdk, private readonly ImmutableConfig $membershipConfig,
                              private readonly LoggerChannelInterface $logger)
  {
  }

  /**
   * Assign the configured default memberships in CKAN for the created CKAN user.
   *
   * @param UserCreated $event The event to process
   */
  public function assignDefaultMemberships(UserCreated $event): void
  {
    $defaultMemberships = $this->membershipConfig->get('default_roles') ?? [];

    if (0 === count($defaultMemberships)) {
      return;
    }

    $grants = 0;

    foreach ($defaultMemberships as $organizationId => $role) {
      try {
        $this->ckanSdk->organizationMemberships()->addUser($organizationId, $event->getUserId(), $role);

        $grants = $grants + 1;
      } catch (ClientException|ResponseException) {
        $this->logger->error('Failed to give CKAN user @user the "@role" role for the @organization CKAN organization', [
          '@user'         => $event->getUserId(),
          '@role'         => $role,
          '@organization' => $organizationId,
        ]);
      }
    }

    if ($grants > 0) {
      $this->logger->info('Granted @count default membership(s) to user with CKAN ID @user', [
        '@count' => $grants,
        '@user'  => $event->getUserId(),
      ]);
    }
  }
}
