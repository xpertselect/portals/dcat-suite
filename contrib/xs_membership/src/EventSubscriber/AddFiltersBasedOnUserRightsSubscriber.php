<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership\EventSubscriber;

use Drupal\xs_membership\MembershipService;
use Drupal\xs_searchable_content\Event\PreparingResultsPerThemeBlock;
use Drupal\xs_searchable_content\Event\PreparingSearchQuery;
use Drupal\xs_searchable_content\Event\PreparingSuggestQuery;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class AddFiltersBasedOnUserRightsSubscriber.
 *
 * Listens to events pertaining to search profiles and adds filters based on the users rights.
 */
final class AddFiltersBasedOnUserRightsSubscriber implements EventSubscriberInterface
{
  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [
      PreparingSuggestQuery::class         => 'addVisibilityFiltersBasedOnRights',
      PreparingSearchQuery::class          => 'addVisibilityFiltersBasedOnRights',
      PreparingResultsPerThemeBlock::class => 'addVisibilityFiltersBasedOnRights',
    ];
  }

  /**
   * SearchProfileSubscriber constructor.
   *
   * @param MembershipService $membershipService The service for reasoning about
   *                                             the rights of a user in CKAN
   */
  public function __construct(private readonly MembershipService $membershipService)
  {
  }

  /**
   * Alters the search profile to add visibility filters based on the rights of the user.
   *
   * @param PreparingSuggestQuery|PreparingSearchQuery|PreparingResultsPerThemeBlock $event The event holding the search profile
   */
  public function addVisibilityFiltersBasedOnRights(PreparingSuggestQuery|PreparingSearchQuery|PreparingResultsPerThemeBlock $event): void
  {
    $event->searchProfile->addFilters($this->buildMembershipFilter());
  }

  /**
   * Builds the membership filters for a user.
   *
   * @return array<int, string> The membership filters for a user
   */
  private function buildMembershipFilter(): array
  {
    $filters = ['visibility:"public"'];

    // Get CKAN organizations with read access for the current user and append
    // them to the $filters
    if ($memberships = $this->membershipService->getMembershipsForUser(keyField: 'name')) {
      $filters[] = sprintf('(visibility:"private" AND organization:("%s"))',
        implode('" OR "', array_keys($memberships))
      );
    }

    return [
      sprintf('(%s)', implode(' OR ', $filters)),
    ];
  }
}
