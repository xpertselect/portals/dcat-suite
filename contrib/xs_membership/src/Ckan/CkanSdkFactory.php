<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership\Ckan;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_ckan\CkanSdkFactory as XsCkanSdkFactory;
use Psr\EventDispatcher\EventDispatcherInterface;
use XpertSelect\CkanSdk\HttpRequestService;

/**
 * Class CkanSdkFactory.
 *
 * Drupal service factory for creating `CkanSdk` instances configured to act as the current Drupal user.
 *
 * This means that the accompanying CKAN user is resolved and its api key is assigned to the CkanSdk such that all
 * requests are performed as said user.
 *
 * @see XsCkanSdkFactory::createSdkInstance
 */
final class CkanSdkFactory
{
  /**
   * Create a CkanSdk instance configured to act as the given Drupal user.
   *
   * @param CkanUserResolver         $userResolver       The Drupal user resolver service
   * @param HttpRequestService       $httpRequestService The HTTP request service the SDK should use
   * @param EventDispatcherInterface $eventDispatcher    The event dispatcher the SDK should use
   * @param CacheBackendInterface    $cacheBackend       The caching backend to assign
   *
   * @return CkanSdk The created instance
   *
   * @see XsCkanSdkFactory::createSdkInstance()
   */
  public static function createForCurrentUser(CkanUserResolver $userResolver, HttpRequestService $httpRequestService,
                                              EventDispatcherInterface $eventDispatcher,
                                              CacheBackendInterface $cacheBackend): CkanSdk
  {
    $ckanUser = $userResolver->loadForCurrentUser();

    if (is_array($ckanUser) && array_key_exists('apikey', $ckanUser)) {
      $httpRequestService->setApiKey(strval($ckanUser['apikey']));
    }

    return XsCkanSdkFactory::createSdkInstance($httpRequestService, $eventDispatcher, $cacheBackend, 'default.user');
  }
}
