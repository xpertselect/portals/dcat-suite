<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_membership\Ckan;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\Entity\User as DrupalUser;
use Drupal\user\UserInterface;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_membership\User;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class CkanUserResolver.
 *
 * A Drupal service that resolves a Drupal user to its accompanying CKAN user.
 */
final class CkanUserResolver
{
  /**
   * The static cache of previously resolved CKAN users.
   *
   * @var array<string, array<string, mixed>>
   */
  private static array $resolverCache = [];

  /**
   * Resets the in-memory cache of resolved CKAN users.
   */
  public static function resetResolverCache(): void
  {
    self::$resolverCache = [];
  }

  /**
   * CkanUserResolver constructor.
   *
   * @param CkanSdk               $ckanSdk      The SDK for interacting with CKAN
   * @param AccountProxyInterface $accountProxy A proxy for the current Drupal user
   * @param RouteMatchInterface   $routeMatch   The Drupal route matching service
   */
  public function __construct(private readonly CkanSdk $ckanSdk, private readonly AccountProxyInterface $accountProxy,
                              private readonly RouteMatchInterface $routeMatch)
  {
  }

  /**
   * Retrieve the CKAN user object from the CKAN API that is related to the given Drupal user.
   *
   * @param UserInterface $user The Drupal user
   *
   * @return null|array<string, mixed> The accompanying CKAN user or null or any error
   */
  public function loadForUser(UserInterface $user): ?array
  {
    try {
      $ckanUsername = User::NAME_PREFIX . $user->id();

      if (array_key_exists($ckanUsername, self::$resolverCache)) {
        return self::$resolverCache[$ckanUsername];
      }

      $ckanUser = $this->ckanSdk->users()->getOrCreate($ckanUsername);

      self::$resolverCache[$ckanUsername] = $ckanUser;

      return $ckanUser;
    } catch (ClientException|ResponseException) {
      return NULL;
    }
  }

  /**
   * Calls `CkanUserResolver::loadForUser()` with the current Drupal user making the request.
   *
   * @return null|array<string, mixed> The accompanying CKAN user or null or any error
   *
   * @see CkanUserResolver::loadForUser()
   */
  public function loadForCurrentUser(): ?array
  {
    $user = DrupalUser::load($this->accountProxy->id());

    if (is_null($user)) {
      return NULL;
    }

    return $this->loadForUser($user);
  }

  /**
   * Calls `CkanUserResolver::loadForUser()` with the Drupal user contained in the URL in the `user` parameter.
   *
   * @return null|array<string, mixed> The accompanying CKAN user or null or any error
   *
   * @see CkanUserResolver::loadForUser()
   */
  public function loadForUserInRoute(): ?array
  {
    $routeUser = $this->routeMatch->getParameter('user');

    if (!$routeUser instanceof UserInterface) {
      return NULL;
    }

    return $this->loadForUser($routeUser);
  }
}
