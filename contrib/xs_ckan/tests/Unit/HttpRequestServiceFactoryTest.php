<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_ckan\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\xs_ckan\HttpRequestServiceFactory;
use Drupal\xs_ckan\XsCkan;
use GuzzleHttp\Client;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_ckan\TestCase;
use XpertSelect\CkanSdk\HttpRequestService;

/**
 * @internal
 */
final class HttpRequestServiceFactoryTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    $guzzleFactory = M::mock(ClientFactory::class, function(MI $mock) {
      $mock->shouldReceive('fromOptions')->andReturn(M::mock(Client::class));
    });

    Assert::assertInstanceOf(
      HttpRequestService::class,
      HttpRequestServiceFactory::createHttpRequestServiceInstanceForEndpoint('http://example.com', $guzzleFactory)
    );
  }

  public function testWhenEndpointIsMissingFromConfigDefaultIsUsed(): void
  {
    $configFactory = M::mock(ConfigFactoryInterface::class, function(MI $mock) {
      $config = M::mock(ImmutableConfig::class, function(MI $mock) {
        $mock->shouldReceive('get')->with('ckan_endpoint')->andReturnNull();
      });

      $mock->shouldReceive('get')->with(XsCkan::SETTINGS_KEY)->andReturn($config);
    });

    $guzzleFactory = M::mock(ClientFactory::class, function(MI $mock) {
      $mock->shouldReceive('fromOptions')->andReturn(M::mock(Client::class));
    });

    $loggerChannelFactory = M::mock(LoggerChannelFactoryInterface::class, function(MI $mock) {
      $loggerChannel = M::mock(LoggerChannel::class, function(MI $mock) {
        $mock->shouldReceive('warning')
          ->andReturnUsing(function(string $message) {
            Assert::assertEquals(
              'The "ckan_endpoint" config value is missing or invalid. Using fallback value "@value".',
              $message
            );
          });
      });

      $mock->shouldReceive('get')->with(XsCkan::LOG_CHANNEL)->andReturn($loggerChannel);
    });

    HttpRequestServiceFactory::createHttpRequestServiceInstance($guzzleFactory, $configFactory, $loggerChannelFactory);
  }

  public function testInstanceCanBeCreatedFromConfig(): void
  {
    $configFactory = M::mock(ConfigFactoryInterface::class, function(MI $mock) {
      $config = M::mock(ImmutableConfig::class, function(MI $mock) {
        $mock->shouldReceive('get')->with('ckan_endpoint')->andReturn('https://example.com/');
      });

      $mock->shouldReceive('get')->with(XsCkan::SETTINGS_KEY)->andReturn($config);
    });

    $guzzleFactory = M::mock(ClientFactory::class, function(MI $mock) {
      $mock->shouldReceive('fromOptions')->andReturn(M::mock(Client::class));
    });

    $loggerFactory = M::mock(LoggerChannelFactoryInterface::class);

    Assert::assertInstanceOf(
      HttpRequestService::class,
      HttpRequestServiceFactory::createHttpRequestServiceInstance($guzzleFactory, $configFactory, $loggerFactory)
    );
  }
}
