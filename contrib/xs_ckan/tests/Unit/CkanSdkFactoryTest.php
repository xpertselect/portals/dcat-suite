<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_ckan\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\xs_ckan\CkanSdk as DrupalCkanSdk;
use Drupal\xs_ckan\CkanSdkFactory;
use Drupal\xs_ckan\XsCkan;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Tests\xs_ckan\TestCase;
use XpertSelect\CkanSdk\CkanSdk;
use XpertSelect\CkanSdk\HttpRequestService;

/**
 * @internal
 */
final class CkanSdkFactoryTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    $httpRequestService = M::mock(HttpRequestService::class);
    $eventDispatcher    = M::mock(EventDispatcherInterface::class);
    $instance           = CkanSdkFactory::createSdkInstance($httpRequestService, $eventDispatcher);

    Assert::assertInstanceOf(CkanSdk::class, $instance);
    Assert::assertInstanceOf(DrupalCkanSdk::class, $instance);
  }

  public function testInstanceIdDefaultsToDefault(): void
  {
    $httpRequestService = M::mock(HttpRequestService::class);
    $eventDispatcher    = M::mock(EventDispatcherInterface::class);
    $instance           = CkanSdkFactory::createSdkInstance($httpRequestService, $eventDispatcher);

    Assert::assertTrue($instance->hasInstanceId());
    Assert::assertEquals('default', $instance->getInstanceId());
  }

  public function testInstanceIdCanBeSpecified(): void
  {
    $httpRequestService = M::mock(HttpRequestService::class);
    $eventDispatcher    = M::mock(EventDispatcherInterface::class);
    $instance           = CkanSdkFactory::createSdkInstance($httpRequestService, $eventDispatcher, instanceId: 'foo');

    Assert::assertTrue($instance->hasInstanceId());
    Assert::assertEquals('foo', $instance->getInstanceId());
  }

  public function testInstanceIdCanBeDisabled(): void
  {
    $httpRequestService = M::mock(HttpRequestService::class);
    $eventDispatcher    = M::mock(EventDispatcherInterface::class);
    $instance           = CkanSdkFactory::createSdkInstance($httpRequestService, $eventDispatcher, instanceId: NULL);

    Assert::assertFalse($instance->hasInstanceId());
    Assert::assertNull($instance->getInstanceId());
  }

  public function testAdminInstanceThrowsExceptionOnMissingConfiguration(): void
  {
    $httpRequestService = M::mock(HttpRequestService::class, function(MI $mock) {
      $mock->shouldReceive('setApiKey')->with('my-secret-key')->andReturn();
    });
    $eventDispatcher    = M::mock(EventDispatcherInterface::class);
    $configFactory      = M::mock(ConfigFactoryInterface::class, function(MI $mock) {
      $config = M::mock(ImmutableConfig::class, function(MI $mock) {
        $mock->shouldReceive('get')->with('ckan_user_api_key')->andReturn('my-secret-key');
      });

      $mock->shouldReceive('get')->with(XsCkan::SETTINGS_KEY)->andReturn($config);
    });

    $instance = CkanSdkFactory::createAdminSdkInstance($configFactory, $httpRequestService, $eventDispatcher);

    Assert::assertInstanceOf(CkanSdk::class, $instance);
    Assert::assertInstanceOf(DrupalCkanSdk::class, $instance);
  }
}
