<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_ckan\Unit\Controller;

use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_ckan\Controller\CkanStatusController;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Tests\xs_ckan\TestCase;

/**
 * @internal
 */
final class CkanStatusControllerTest extends TestCase
{
  public function testControllerCanBeCreated(): void
  {
    try {
      $container = M::mock(ContainerInterface::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->with('xs_ckan.ckan_sdk.admin')
          ->andReturn(M::mock(CkanSdk::class));
      });

      Assert::assertInstanceOf(
        CkanStatusController::class,
        CkanStatusController::create($container)
      );
    } catch (ServiceNotFoundException|ServiceCircularReferenceException $e) {
      $this->fail($e->getMessage());
    }
  }
}
