<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_ckan\Unit;

use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_ckan\Repository\OrganizationRepository;
use Drupal\xs_ckan\Repository\UserRepository;
use Mockery as M;
use PHPUnit\Framework\Assert;
use Tests\xs_ckan\TestCase;
use XpertSelect\CkanSdk\HttpRequestService;

/**
 * @internal
 */
final class CkanSdkTest extends TestCase
{
  public function testSdkOffersCustomOrganizationRepository(): void
  {
    $requestService = M::mock(HttpRequestService::class);
    $sdk            = new CkanSdk($requestService);
    $repository     = $sdk->organizations();

    Assert::assertInstanceOf(OrganizationRepository::class, $repository);
  }

  public function testSdkOffersCustomUserRepository(): void
  {
    $requestService = M::mock(HttpRequestService::class);
    $sdk            = new CkanSdk($requestService);
    $repository     = $sdk->users();

    Assert::assertInstanceOf(UserRepository::class, $repository);
  }
}
