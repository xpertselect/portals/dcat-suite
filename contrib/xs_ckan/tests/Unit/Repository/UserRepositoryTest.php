<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_ckan\Unit\Repository;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\xs_ckan\Repository\UserRepository;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use stdClass;
use Tests\xs_ckan\TestCase;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
final class UserRepositoryTest extends TestCase
{
  public function testListReturnsCachedDataWhenAvailable(): void
  {
    try {
      $httpService     = M::mock(HttpRequestService::class);
      $eventDispatcher = M::mock(EventDispatcherInterface::class);
      $cacheBackend    = M::mock(CacheBackendInterface::class, function(MI $mock) {
        $cachedData       = new stdClass();
        $cachedData->data = ['foo' => 'bar'];

        $mock->shouldReceive('get')
          ->with('xs_ckan.repository.user.get.foo')
          ->andReturn($cachedData);
      });

      $repository = new UserRepository($httpService, $eventDispatcher, $cacheBackend);

      Assert::assertEquals(['foo' => 'bar'], $repository->get('foo'));
    } catch (ClientException|ResponseException $e) {
      $this->fail($e->getMessage());
    }
  }
}
