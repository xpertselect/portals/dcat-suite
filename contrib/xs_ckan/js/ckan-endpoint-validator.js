/* globals Drupal, jQuery, drupalSettings */

(function ($, Drupal, drupalSettings) {
  "use strict";

  let lastValidatedValue = null;

  function validateEndpoint(endpointInput) {
    let valueToValidate = endpointInput.val();

    if (!valueToValidate) {
      return;
    }

    if (valueToValidate === lastValidatedValue) {
      return;
    }

    lastValidatedValue = valueToValidate;

    if (!validateUrl(valueToValidate)) {
      $("<div class=\"invalidCkanEndpointMessage\">" +
        "  <small><strong>&Cross;</strong> " + Drupal.t("This is not a valid URL!") + "</small>" +
        "</div>").insertAfter(endpointInput);

      return;
    }

    $("<div class=\"ckanEndpointValidating\">" +
      "  <small><span class=\"ckanEndpointValidatingSpinner\"></span> " + Drupal.t("Validating...") + "</small>" +
      "</div>").insertAfter(endpointInput);

    $.ajax({
      method: "POST",
      url: drupalSettings.xs_ckan.endpoint_validation.validator,
      data: {"ckan_endpoint": valueToValidate},
      accepts: {"Content-Type": "application/json"},
      success: function (data) {
        $(".ckanEndpointValidating").remove();

        if (data.valid) {
          endpointInput.addClass("validCkanEndpoint");
          const message = Drupal.t("The CKAN endpoint <em>@endpoint</em> is accessible!", {
            "@endpoint": valueToValidate
          });

          $("<div class=\"validCkanEndpointMessage\">" +
            "  <small><strong>&check;</strong> " + message + "</small>" +
            "</div>").insertAfter(endpointInput);
        } else {
          endpointInput.addClass("invalidCkanEndpoint");
          const message = Drupal.t("The CKAN endpoint <em>@endpoint</em> could not be reached!", {
            "@endpoint": valueToValidate
          });

          $("<div class=\"invalidCkanEndpointMessage\">" +
            "  <small><strong>&Cross;</strong> " + message + "</small>" +
            "</div>").insertAfter(endpointInput);
        }
      }
    });
  }

  function validateUrl(input) {
    try {
      const newUrl = new URL(input);

      return newUrl.protocol === "http:" || newUrl.protocol === "https:";
    } catch (err) {
      return false;
    }
  }

  Drupal.behaviors.ckanEndpointValidation = {
    attach: function () {
      const endpointInput = $("." + drupalSettings.xs_ckan.endpoint_validation.selector);

      endpointInput.change(function() {
        endpointInput.removeClass("validCkanEndpoint invalidCkanEndpoint");
        $(".validCkanEndpointMessage, .invalidCkanEndpointMessage").remove();
      });

      endpointInput.focusout(function() {
        validateEndpoint(endpointInput);
      });

      validateEndpoint(endpointInput);
    }
  };
})(jQuery, Drupal, drupalSettings);
