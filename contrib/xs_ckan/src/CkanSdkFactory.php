<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_ckan;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Psr\EventDispatcher\EventDispatcherInterface;
use XpertSelect\CkanSdk\HttpRequestService;

/**
 * Class CkanSdkFactory.
 *
 * Factory class for creating CkanSdk instances.
 *
 * @see CkanSdk
 */
final class CkanSdkFactory
{
  /**
   * Creates a CkanSdk instance for a given endpoint.
   *
   * @param string               $endpoint      The endpoint of the CKAN API
   * @param ClientFactory        $guzzleFactory The factory for creating Guzzle clients
   * @param array<string, mixed> $guzzleOptions The options to give to the Guzzle client
   *
   * @return CkanSdk The created instance
   */
  public static function createSdkInstanceForEndpoint(string $endpoint, ClientFactory $guzzleFactory,
                                                      array $guzzleOptions = []): CkanSdk
  {
    return new CkanSdk(
      HttpRequestServiceFactory::createHttpRequestServiceInstanceForEndpoint($endpoint, $guzzleFactory, $guzzleOptions)
    );
  }

  /**
   * Create a CkanSdk instance with the given arguments.
   *
   * @param HttpRequestService         $httpRequestService The HTTP request service the SDK should use
   * @param EventDispatcherInterface   $eventDispatcher    The event dispatcher the SDK should use
   * @param null|CacheBackendInterface $cacheBackend       The optional caching backend to assign
   * @param null|string                $instanceId         The ID of the CKAN instance
   *
   * @return CkanSdk The created instance
   */
  public static function createSdkInstance(HttpRequestService $httpRequestService,
                                           EventDispatcherInterface $eventDispatcher,
                                           ?CacheBackendInterface $cacheBackend = NULL,
                                           ?string $instanceId = 'default'): CkanSdk
  {
    return new CkanSdk($httpRequestService, $eventDispatcher, $cacheBackend, $instanceId);
  }

  /**
   * Create a CkanSdk instance with a CKAN sysadmin API key assigned to it.
   *
   * @param ConfigFactoryInterface     $configFactory      The configuration factory holding the admin settings
   * @param HttpRequestService         $httpRequestService The HTTP request service the SDK should use
   * @param EventDispatcherInterface   $eventDispatcher    The event dispatcher the SDK should use
   * @param null|CacheBackendInterface $cacheBackend       The optional caching backend to assign
   *
   * @return CkanSdk The created instance
   *
   * @see CkanSdkFactory::createSdkInstance()
   */
  public static function createAdminSdkInstance(ConfigFactoryInterface $configFactory,
                                                HttpRequestService $httpRequestService,
                                                EventDispatcherInterface $eventDispatcher,
                                                ?CacheBackendInterface $cacheBackend = NULL): CkanSdk
  {
    $ckanSettings = $configFactory->get(XsCkan::SETTINGS_KEY);

    if ($apiKey = $ckanSettings->get('ckan_user_api_key')) {
      $httpRequestService->setApiKey($apiKey);
    }

    return self::createSdkInstance($httpRequestService, $eventDispatcher, $cacheBackend, 'default.admin');
  }
}
