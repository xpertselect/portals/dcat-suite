<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_ckan;

/**
 * Class Tag.
 *
 * Provides several constants related to the tags that can be (or are) stored in CKAN.
 */
final class Tag
{
  /**
   * The minimum length of a CKAN tag.
   *
   * @var int
   */
  public const MIN_LENGTH = 2;

  /**
   * The maximum length of a CKAN tag.
   *
   * @var int
   */
  public const MAX_LENGTH = 100;

  /**
   * The regex pattern that can be used to validate the contents of a tag.
   *
   * @var string
   */
  public const TAG_PATTERN = '^[a-zA-Z0-9-_\s]+$';

  /**
   * The regex pattern that can be used to validate a collection of tags as a single string (AKA `tag_string`).
   *
   * @var string
   */
  public const TAG_STRING_PATTERN = '[a-zA-Z0-9-_\s]*(?:[,]\s*[a-zA-Z0-9-_\s]+)*';
}
