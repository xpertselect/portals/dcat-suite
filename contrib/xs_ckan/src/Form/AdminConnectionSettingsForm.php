<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_ckan\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\xs_ckan\XsCkan;
use Drupal\xs_general\ManagedSettingsTrait;

/**
 * Class AdminConnectionSettingsForm.
 *
 * A Drupal configuration form for managing the connection settings used to communicate with a CKAN instance.
 */
final class AdminConnectionSettingsForm extends ConfigFormBase
{
  use ManagedSettingsTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'xs_ckan_admin_connection_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    if ($this->isManaged(XsCkan::SETTINGS_KEY, TRUE)) {
      return $form;
    }

    $config = $this->config(XsCkan::SETTINGS_KEY);

    $form['ckan_connection'] = [
      '#type'                => 'fieldset',
      '#title'               => $this->t('CKAN connection settings'),
      '#description'         => $this->t('The settings used to connect to the CKAN instance that is servicing this XpertSelect Portals stack.'),
      '#description_display' => 'before',
      '#open'                => TRUE,

      'ckan_endpoint' => [
        '#type'                => 'url',
        '#title'               => $this->t('HTTP(S) endpoint'),
        '#description'         => $this->t('Provide the HTTP(S) address of the CKAN instance.'),
        '#description_display' => 'before',
        '#placeholder'         => 'https://',
        '#default_value'       => $config->get('ckan_endpoint'),
        '#attributes'          => ['class' => ['ckanEndpoint']],
      ],
    ];

    $form['ckan_authorization'] = [
      '#type'                => 'fieldset',
      '#title'               => $this->t('CKAN Sysadmin'),
      '#description'         => $this->t('Configure the CKAN sysadmin user that this XpertSelect Portals installation will use for the management of users and their authorizations.'),
      '#description_display' => 'before',
      '#open'                => TRUE,

      'ckan_user_id' => [
        '#type'                => 'textfield',
        '#title'               => $this->t('Sysadmin ID'),
        '#description'         => $this->t('Provide the user ID of the CKAN sysadmin user.'),
        '#description_display' => 'before',
        '#default_value'       => $config->get('ckan_user_id'),
      ],

      'ckan_user_api_key' => [
        '#type'                => 'textfield',
        '#title'               => $this->t('Sysadmin API key'),
        '#description'         => $this->t('Provide the API key of the CKAN sysadmin user.'),
        '#description_display' => 'before',
        '#default_value'       => $config->get('ckan_user_api_key'),
      ],
    ];

    $form['#attached']['library'][]                                        = 'xs_ckan/ckan_endpoint_validator';
    $form['#attached']['drupalSettings']['xs_ckan']['endpoint_validation'] = [
      'validator' => Url::fromRoute('xs_ckan.ajax.ckan_endpoint_validator', [], ['absolute' => FALSE])->toString(),
      'selector'  => 'ckanEndpoint',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    if ($this->isManaged(XsCkan::SETTINGS_KEY, TRUE)) {
      return;
    }

    $this->config(XsCkan::SETTINGS_KEY)
      ->set('ckan_endpoint', $form_state->getValue('ckan_endpoint'))
      ->set('ckan_user_id', $form_state->getValue('ckan_user_id'))
      ->set('ckan_user_api_key', $form_state->getValue('ckan_user_api_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [XsCkan::SETTINGS_KEY];
  }
}
