<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_ckan;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\xs_ckan\Repository\OrganizationRepository;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class CkanCacheRefresher.
 *
 * A Drupal service that is responsible for refreshing the various CKAN caches stored in Drupal in regular intervals.
 * This service should be triggered by a cron hook. This service will determine whether the caches should be refreshed.
 *
 * @see xs_ckan_cron()
 */
final class CkanCacheRefresher
{
  /**
   * The state ID containing the timestamp of the last cache refresh.
   *
   * @var string
   */
  public const STATE_ID = 'xs_ckan.last_cron_check';

  /**
   * The interval at which the CKAN caches should be refreshed.
   *
   * @var int
   */
  public const REFRESH_INTERVAL = 60 * 60;

  /**
   * CkanCacheRefresher constructor.
   *
   * @param StateInterface                $drupalState The Drupal state service
   * @param TimeInterface                 $drupalTime  The Drupal time service
   * @param CacheBackendInterface         $cache       The Drupal cache backend
   * @param LoggerChannelFactoryInterface $logger      The logging factory
   * @param CkanSdk                       $ckanSdk     The SDK for interacting with CKAN
   */
  public function __construct(private readonly StateInterface $drupalState, private readonly TimeInterface $drupalTime,
                              private readonly CacheBackendInterface $cache,
                              private readonly LoggerChannelFactoryInterface $logger, private readonly CkanSdk $ckanSdk)
  {
  }

  /**
   * Refresh the CKAN caches stored in Drupal.
   */
  public function refreshCache(): void
  {
    if (!$this->shouldRefreshCache()) {
      return;
    }

    $this->updateState();
    $this->refreshOrganizationListCache();
  }

  /**
   * Update the state to set the last check to the current timestamp.
   */
  public function updateState(): void
  {
    $this->drupalState->set(self::STATE_ID, $this->drupalTime->getRequestTime());
  }

  /**
   * Determine if the cache should be refreshed.
   *
   * @return bool Whether the cache should be refreshed
   */
  private function shouldRefreshCache(): bool
  {
    $sinceLastCheck = $this->drupalTime->getRequestTime() - $this->drupalState->get(self::STATE_ID, 0);

    return $sinceLastCheck >= self::REFRESH_INTERVAL;
  }

  /**
   * Refresh the organization list cache.
   */
  private function refreshOrganizationListCache(): void
  {
    try {
      $this->cache->invalidate(OrganizationRepository::LIST_CACHE_ID);
      $this->ckanSdk->organizations()->list();
    } catch (ClientException|ResponseException $e) {
      $this->logger->get(XsCkan::LOG_CHANNEL)->error('Failed to refresh the CKAN organization list cache; @error', [
        '@error' => $e->getMessage(),
      ]);
    }
  }
}
