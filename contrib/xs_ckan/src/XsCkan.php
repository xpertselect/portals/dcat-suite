<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_ckan;

/**
 * Class XsCkan.
 *
 * Simple PHP class holding various constants used by the `xs_ckan` Drupal module.
 */
final class XsCkan
{
  /**
   * The key identifying the `xs_ckan` settings in Drupal.
   *
   * @var string
   */
  public const SETTINGS_KEY = 'xs_ckan.settings';

  /**
   * The log channel that should be used by this Drupal module.
   *
   * @var string
   */
  public const LOG_CHANNEL = 'xs_ckan';
}
