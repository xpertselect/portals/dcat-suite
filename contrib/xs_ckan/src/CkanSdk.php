<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_ckan;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\xs_ckan\Repository\OrganizationRepository;
use Drupal\xs_ckan\Repository\UserRepository;
use Psr\EventDispatcher\EventDispatcherInterface;
use XpertSelect\CkanSdk\CkanSdk as BaseCkanSdk;
use XpertSelect\CkanSdk\HttpRequestService;

/**
 * Class CkanSdk.
 *
 * Augments the standard CkanSdk by providing custom implementations for several repositories that introduce the use of
 * caching to reduce the need to communicate with the CKAN API directly.
 */
final class CkanSdk extends BaseCkanSdk
{
  /**
   * CkanSdk constructor.
   *
   * @param HttpRequestService            $requestService  The service for interacting with the HTTP api
   * @param null|EventDispatcherInterface $eventDispatcher The service for dispatching events
   * @param null|CacheBackendInterface    $cacheBackend    The caching implementation to assign
   * @param null|string                   $instanceId      The ID of the CKAN instance
   */
  public function __construct(HttpRequestService $requestService, ?EventDispatcherInterface $eventDispatcher = NULL,
                              private readonly ?CacheBackendInterface $cacheBackend = NULL, ?string $instanceId = NULL)
  {
    parent::__construct($requestService, $eventDispatcher, $instanceId);
  }

  /**
   * {@inheritdoc}
   */
  protected function makeOrganizationRepository(): OrganizationRepository
  {
    return new OrganizationRepository($this->requestService, $this->cacheBackend, $this->instanceId);
  }

  /**
   * {@inheritdoc}
   */
  protected function makeUserRepository(): UserRepository
  {
    return new UserRepository($this->requestService, $this->eventDispatcher, $this->cacheBackend, $this->instanceId);
  }
}
