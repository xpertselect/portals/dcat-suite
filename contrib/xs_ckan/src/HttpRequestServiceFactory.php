<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_ckan;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Psr7\HttpFactory;
use XpertSelect\CkanSdk\HttpRequestService;

/**
 * Class HttpRequestServiceFactory.
 *
 * Factory class for creating HttpRequestService instances.
 *
 * @see HttpRequestService
 */
final class HttpRequestServiceFactory
{
  /**
   * A sensible default value for the ckan_endpoint when no such endpoint has been configured.
   *
   * @var string
   */
  public const FALLBACK_ENDPOINT = 'http://localhost:5000';

  /**
   * Creates a HttpRequestService instance for the given endpoint.
   *
   * @param string               $endpoint      The endpoint of the CKAN API
   * @param ClientFactory        $guzzleFactory The factory for creating Guzzle client instances
   * @param array<string, mixed> $guzzleOptions Options to give to the Guzzle client
   *
   * @return HttpRequestService The created instance
   */
  public static function createHttpRequestServiceInstanceForEndpoint(string $endpoint,
                                                                     ClientFactory $guzzleFactory,
                                                                     array $guzzleOptions = []): HttpRequestService
  {
    $httpFactory = new HttpFactory();

    return new HttpRequestService(
      $endpoint,
      $guzzleFactory->fromOptions($guzzleOptions),
      $httpFactory,
      $httpFactory
    );
  }

  /**
   * Creates a HttpRequestService instance using the Guzzle client provided by Drupal.
   *
   * @param ClientFactory                 $guzzleFactory The factory for creating Guzzle client instances
   * @param ConfigFactoryInterface        $configFactory The configuration factory holding the CKAN endpoint value
   * @param LoggerChannelFactoryInterface $loggerFactory The factory for creating log channel instances
   *
   * @return HttpRequestService The created instance
   */
  public static function createHttpRequestServiceInstance(ClientFactory $guzzleFactory,
                                                          ConfigFactoryInterface $configFactory,
                                                          LoggerChannelFactoryInterface $loggerFactory): HttpRequestService
  {
    $endpoint = $configFactory->get(XsCkan::SETTINGS_KEY)->get('ckan_endpoint');

    if (!is_string($endpoint)) {
      $loggerFactory->get(XsCkan::LOG_CHANNEL)
        ->warning('The "ckan_endpoint" config value is missing or invalid. Using fallback value "@value".', [
          '@value' => self::FALLBACK_ENDPOINT,
        ]);

      $endpoint = self::FALLBACK_ENDPOINT;
    }

    return self::createHttpRequestServiceInstanceForEndpoint($endpoint, $guzzleFactory, [
      'connect_timeout' => 2,
      'timeout'         => 10,
    ]);
  }
}
