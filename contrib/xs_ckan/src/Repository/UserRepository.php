<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_ckan\Repository;

use Drupal\Core\Cache\CacheBackendInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\Repository\UserRepository as BaseUserRepository;

/**
 * Class UserRepository.
 *
 * Augments the standard CKAN SDK UserRepository with caching functionality to reduce the need to communicate with a
 * CKAN instance.
 */
final class UserRepository extends BaseUserRepository
{
  /**
   * UserRepository constructor.
   *
   * @param HttpRequestService            $httpService     The service for interacting with the HTTP API
   * @param null|EventDispatcherInterface $eventDispatcher The event dispatcher to assign
   * @param null|CacheBackendInterface    $cacheBackend    The caching implementation to use
   * @param ?string                       $instanceId
   */
  public function __construct(HttpRequestService $httpService, ?EventDispatcherInterface $eventDispatcher = NULL,
                              private readonly ?CacheBackendInterface $cacheBackend = NULL, ?string $instanceId = NULL)
  {
    parent::__construct($httpService, $eventDispatcher, $instanceId);
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $nameOrId): array
  {
    if (is_null($this->cacheBackend)) {
      return parent::get($nameOrId);
    }

    $cacheId = sprintf('xs_ckan.repository.user.get.%s', $nameOrId);

    if (($user = $this->cacheBackend->get($cacheId)) && isset($user->data)) {
      // @phpstan-ignore-next-line
      return (array) $user->data;
    }

    $user = parent::get($nameOrId);

    $this->cacheBackend->set($cacheId, $user, time() + 60 * 60);

    return $user;
  }
}
