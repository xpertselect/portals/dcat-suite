<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_ckan\Repository;

use Drupal\Core\Cache\CacheBackendInterface;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\Repository\OrganizationRepository as BaseOrganizationRepository;

/**
 * Class OrganizationRepository.
 *
 * Augments the standard CKAN SDK OrganizationRepository with caching functionality to reduce the need to communicate
 * with a CKAN instance.
 */
final class OrganizationRepository extends BaseOrganizationRepository
{
  /**
   * The cache ID holding the output of the `list()` method.
   *
   * @var string
   */
  public const LIST_CACHE_ID = 'xs_ckan.repository.organization.list';

  /**
   * OrganizationRepository constructor.
   *
   * @param HttpRequestService         $httpService  The service for interacting with the HTTP API
   * @param null|CacheBackendInterface $cacheBackend The caching implementation to use
   * @param null|string                $instanceId   The ID of the CKAN instance
   */
  public function __construct(HttpRequestService $httpService,
                              private readonly ?CacheBackendInterface $cacheBackend = NULL, ?string $instanceId = NULL)
  {
    parent::__construct($httpService, $instanceId);
  }

  /**
   * {@inheritdoc}
   */
  public function list(): array
  {
    if (is_null($this->cacheBackend)) {
      return parent::list();
    }

    if (($organizations = $this->cacheBackend->get(self::LIST_CACHE_ID)) && isset($organizations->data)) {
      return (array) $organizations->data;
    }

    if ($organizations = parent::list()) {
      $this->cacheBackend->set(self::LIST_CACHE_ID, $organizations);

      return $organizations;
    }

    return [];
  }
}
