<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_ckan\Controller;

use Drupal\Core\Config\Config;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\xs_ckan\CkanSdk;
use Drupal\xs_ckan\XsCkan;
use Symfony\Component\DependencyInjection\ContainerInterface;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class CkanStatusController.
 *
 * Provides a dashboard page detailing the status of the connected CKAN instance.
 */
final class CkanStatusController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): CkanStatusController
  {
    /** @var CkanSdk $ckanSdk */
    $ckanSdk = $container->get('xs_ckan.ckan_sdk.admin');

    return new CkanStatusController($ckanSdk);
  }

  /**
   * CkanStatusController constructor.
   *
   * @param CkanSdk $ckanSdk The CkanSdk configured with a CKAN sysadmin API key
   */
  public function __construct(private readonly CkanSdk $ckanSdk)
  {
  }

  /**
   * Renders the CKAN status admin page.
   *
   * @return array<string, mixed> The Drupal render array
   */
  public function render(): array
  {
    $config = $this->config(XsCkan::SETTINGS_KEY);

    return [
      'status'    => $this->createStatusTable($config),
      'config'    => $this->createConfigurationTable($config),
      'contents'  => $this->createContentsTable(),
      '#attached' => ['library' => ['xs_general/number-formatter', 'xs_ckan/ckan_status_page']],
    ];
  }

  /**
   * Creates a Drupal render array for displaying the status of the CKAN connection in an HTML table.
   *
   * @param Config $config The instance holding the xs_ckan configuration
   *
   * @return array<string, mixed> The status table render array
   */
  private function createStatusTable(Config $config): array
  {
    return [
      '#theme'     => 'table',
      '#colgroups' => [[['class' => 'ckan-header-cell'], ['class' => 'ckan-data-cell']]],
      '#header'    => [['colspan' => 2, 'data' => $this->t('Connection status')]],
      '#rows'      => array_merge(
        [[$this->t('Endpoint'), $this->getCkanEndpointCell($config)]],
        $this->getCkanStatusRows()
      ),
    ];
  }

  /**
   * Create the value for the table cell holding the CKAN endpoint.
   *
   * @param Config $config The instance holding the xs_ckan configuration
   *
   * @return TranslatableMarkup|array The value of the cell displaying the CKAN endpoint
   */
  private function getCkanEndpointCell(Config $config): TranslatableMarkup|array
  {
    $endpoint = $config->get('ckan_endpoint');

    if (empty($endpoint)) {
      return $this->t('Not configured');
    }

    return [
      'data' => [
        '#type'       => 'link',
        '#title'      => str_replace(['http://', 'https://'], '', $endpoint),
        '#url'        => Url::fromUri($endpoint),
        '#target'     => '_blank',
        '#attributes' => ['target' => '_blank', 'rel' => 'noopener'],
      ],
    ];
  }

  /**
   * Retrieve information about the status of the connected CKAN instance as Drupal table rows.
   *
   * @return array<int, array<int, mixed>> The table rows
   */
  private function getCkanStatusRows(): array
  {
    try {
      $status     = $this->ckanSdk->core()->status();
      $extensions = $status['extensions'];
      $rows       = [
        [$this->t('Status'), $this->t('Available')],
        [$this->t('Version'), $this->getCkanVersionLinkCell($status['ckan_version'])],
      ];

      if (count($extensions) > 0) {
        $rows[] = [
          ['rowspan' => count($extensions), 'data' => $this->t('Extensions')],
          $extensions[0],
        ];

        array_shift($extensions);

        foreach ($extensions as $extension) {
          $rows[] = [$extension];
        }
      }

      return $rows;
    } catch (ClientException|ResponseException $e) {
      $status = $e instanceof ResponseException
        ? 'HTTP ' . $e->response->getPsrResponse()->getStatusCode()
        : $e->getMessage();

      return [[$this->t('Status'), $this->t('Unavailable (@status)', ['@status' => $status])]];
    }
  }

  /**
   * Creates a link to the CKAN release on GitHub that will be used as the value of the CKAN version cell.
   *
   * @param string $version The version of CKAN
   *
   * @return array<string, array<string, mixed>> The render array for the table cell
   */
  private function getCkanVersionLinkCell(string $version): array
  {
    return [
      'data' => [
        '#type'       => 'link',
        '#title'      => $version,
        '#url'        => Url::fromUri(sprintf('https://github.com/ckan/ckan/tree/ckan-%s', $version)),
        '#attributes' => ['target' => '_blank', 'rel' => 'noopener'],
      ],
    ];
  }

  /**
   * Creates a Drupal render array for displaying the configuration of the CKAN Sysadmin user used for membership
   * related interactions.
   *
   * @param Config $config The instance holding the xs_ckan configuration
   *
   * @return array<string, mixed> The configuration table render array
   */
  private function createConfigurationTable(Config $config): array
  {
    return [
      '#theme'     => 'table',
      '#colgroups' => [[['class' => 'ckan-header-cell'], ['class' => 'ckan-data-cell']]],
      '#header'    => [['colspan' => 2, 'data' => $this->t('CKAN Sysadmin')]],
      '#rows'      => [[
        $this->t('Sysadmin ID'),
        $this->getSysadminUserCell($config),
      ], [
        $this->t('Sysadmin API key'),
        !empty($config->get('ckan_user_api_key')) ? $this->t('HIDDEN') : $this->t('Not configured'),
      ]],
    ];
  }

  /**
   * Create the value that will be displayed in the CKAN Sysadmin ID table cell.
   *
   * @param Config $config The instance holding the xs_ckan configuration
   *
   * @return TranslatableMarkup|string The value of the Sysadmin ID cell
   */
  private function getSysadminUserCell(Config $config): TranslatableMarkup|string
  {
    $userId = $config->get('ckan_user_id');

    if (empty($userId)) {
      return $this->t('Not configured');
    }

    try {
      return sprintf('%s (%s)', $this->ckanSdk->users()->get($userId)['name'], $userId);
    } catch (ClientException|ResponseException) {
      return $userId;
    }
  }

  /**
   * Creates a Drupal render array for displaying the contents of the CKAN instance.
   *
   * @return array<string, mixed> The configuration table render array
   */
  private function createContentsTable(): array
  {
    return [
      '#theme'     => 'table',
      '#colgroups' => [[['class' => 'ckan-header-cell'], ['class' => 'ckan-data-cell']]],
      '#header'    => [['colspan' => 2, 'data' => $this->t('CKAN Content')]],
      '#rows'      => $this->getCkanStatisticsRows(),
    ];
  }

  /**
   * Retrieve statistics about the connected CKAN instance as Drupal table rows.
   *
   * @return array<int, array<int, mixed>> The table rows
   */
  private function getCkanStatisticsRows(): array
  {
    try {
      $rows    = [];
      $results = $this->ckanSdk->datasets()->search([
        'rows'            => 0,
        'include_private' => TRUE,
        'facet'           => TRUE,
        'facet.field'     => '["capacity"]',
      ]);

      if (is_null($results['facets']) || !array_key_exists('capacity', $results['facets'])) {
        return $rows;
      }

      $rowspan = array_key_exists('private', $results['facets']['capacity']) ? 2 : 1;

      if (array_key_exists('public', $results['facets']['capacity'])) {
        $rows[] = [
          ['rowspan' => $rowspan, 'data' => $this->t('Datasets')],
          ['data' => ['#markup' => sprintf('Public: <span class="formatNumber">%s</span>',
            $results['facets']['capacity']['public']
          )]],
        ];
      }

      if (array_key_exists('private', $results['facets']['capacity'])) {
        $rows[] = [['data' => ['#markup' => sprintf('Private: <span class="formatNumber">%s</span>',
          $results['facets']['capacity']['private']
        )]]];
      }

      $rows[] = [$this->t('Organizations'), count($this->ckanSdk->organizations()->list())];
      $rows[] = [$this->t('Users'), count($this->ckanSdk->users()->list(FALSE))];

      return $rows;
    } catch (ClientException|ResponseException) {
      return [];
    }
  }
}
