<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_dcat_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_ckan\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Http\ClientFactory;
use Drupal\xs_ckan\CkanSdkFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class CkanEndpointValidator.
 *
 * Provides an endpoint that can validate a given CKAN address. Validation is performed by attempting to request the
 * CKAN status via its `"api/3/action/status_show"` API call. If this request fails for any reason, the endpoint is
 * considered invalid.
 */
final class CkanEndpointValidator extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var ClientFactory $guzzleFactory */
    $guzzleFactory = $container->get('http_client_factory');

    return new self($guzzleFactory);
  }

  /**
   * CkanEndpointValidator constructor.
   *
   * @param ClientFactory $guzzleFactory The factory for creating Guzzle clients
   */
  public function __construct(private readonly ClientFactory $guzzleFactory)
  {
  }

  /**
   * Validates a CKAN endpoint found in the "ckan_endpoint" requests data.
   *
   * @param Request $request The incoming request
   *
   * @return JsonResponse The appropriate JSON response
   */
  public function validate(Request $request): JsonResponse
  {
    $endpoint = $request->get('ckan_endpoint');

    if (empty($endpoint)) {
      return new JsonResponse(['valid' => FALSE]);
    }

    $ckanSdk = CkanSdkFactory::createSdkInstanceForEndpoint($endpoint, $this->guzzleFactory, [
      'connect_timeout' => 2,
      'timeout'         => 2,
    ]);

    try {
      $ckanSdk->core()->status();

      return new JsonResponse(['valid' => TRUE]);
    } catch (ClientException|ResponseException) {
      return new JsonResponse(['valid' => FALSE]);
    }
  }
}
