# XpertSelect / Portals / DCAT Suite

[gitlab.com/xpertselect/portals/dcat-suite](https://gitlab.com/xpertselect/portals/dcat-suite)

The suite of modules required to integrate all DCAT related features into a XpertSelect Portals stack.

## License

View the `LICENSE.md` file for licensing details.

## Installation

Installation of [`xpertselect-portals/xsp_dcat_suite`](https://packagist.org/packages/xpertselect-portals/xsp_dcat_suite) is done via [Composer](https://getcomposer.org).

```shell
composer require xpertselect-portals/xsp_dcat_suite
```
