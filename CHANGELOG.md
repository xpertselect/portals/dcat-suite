# Changelog

## Unreleased (`dev-main`)

### Added

### Changed

### Fixed

---

## Release 1.2.0

### Changed

- The trait `DatasetUtilities` has been removed. Its functionality is moved to `xpertselect/ckan-sdk`.

---

## Release 1.1.0

### Added

- A new form has been added to manually synchronize all datasets with the Dutch national dataportal.
- A new trait `DatasetUtilities` has been added that contains common dataset utility functions.

---

## Release 1.0.1

### Changed

- Updated various composer dependencies.

---

## Release 1.0.0

### Changed

- Upgraded to php8.2.

---

## Release 0.8.0

### Changed

- Increase the timeout of the HTTP request service.

---

## Release 0.7.1

### Changed

- Alter the donl tag tooltip to a bootstrap tooltip.

---

## Release 0.7.0

### Added

- The `xs_membership` module adds the membership filters of a user to the search profile when the `PreparingSearchQuery` event is fired.
- The `xs_membership` module adds the membership filters of a user to the search profile when the `PreparingResultsPerThemeBlock` event is fired.

---

## Release 0.6.0

### Added

- The `xs_membership` module adds the membership filters of a user to the search profile when the `PreparingSuggestQuery` event is fired.

---

## Release 0.5.0

### Added

- The settings form of the `xs_ckan` module now performs live validation of the entered CKAN endpoint.

---

## Release 0.4.0

### Added

- The `xs_donl_connect` module can now publish and delete datasets to and from the Dutch national dataportal.
- The `xs_donl_connect` module now dynamically inserts the reference to the dataset on the Dutch national dataportal on dataset pages.

---

## Release 0.3.1

### Fixed

- The settings form of the `xs_donl_connect` module now correctly references the existing configuration values contained within the `xs_donl_connect.settings.connection` mapping.

---

## Release 0.3.0

### Added

- The settings of the `xs_donl_connect` can now be managed via a form offered to system administrators.

---

## Release 0.2.0

### Added

- Introduced the framework for the `xs_donl_connect` module.

### Fixed

- Corrected the path of the `xs_ckan` translation file.

---

## Release 0.1.0

### Added

- Ported the `xs_ckan` and `xs_membership` modules from the `drupal-suite` repository.
- Initial project setup.
